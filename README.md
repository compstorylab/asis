# Spatial Socioeconomic Study of Mortality Risks in HK

Human mortality is in part a function of multiple socioeconomic factors that differ both spatially and temporally.
Adjusting for other covariates, the human lifespan is positively associated with household wealth.
However, the extent to which mortality in a geographical region is a function of socioeconomic factors in both that region and its neighbors is unclear.
There is also little information on the temporal components of this relationship.

Using the districts of Hong Kong over multiple census years as a case study, 
we demonstrate that there are differences in how wealth indicator variables are associated with longevity in 
(a) areas that are affluent but neighbored by socially deprived districts 
versus (b) wealthy areas surrounded by similarly wealthy districts.

We also show that the inclusion of spatially-distributed variables reduces uncertainty in mortality rate predictions 
in each census year when compared with a baseline model. 
Our results suggest that geographic mortality models should incorporate nonlocal information 
(e.g., spatial neighbors) to lower the variance of their mortality estimates,
and point to a more in-depth analysis of sociospatial spillover effects on mortality rates. 


## Installation

Get started by cloning the repo:
```bash
git clone https://gitlab.com/compstorylab/asis.git
```

### Dependencies

We recommend using [Anaconda](https://docs.anaconda.com/anaconda/install/) to install this list of libraries 

- pytorch
- pyro-ppl
- geopandas 
- networkx
- graphviz
- seaborn
- tqdm

## Usage

```bash
usage: analysis.py [-h] {optional arguments} {positional argument} ...

Spatial socioeconomic study of mortality risks in Hong Kong 
Copyright (c) 2020 The Computational Story Lab. 
Licensed under the MIT License;

positional arguments:
   crosstab           combine datasets at the district level
   heatmap            plot a heatmap of Hong Kong
   network            plot a network of Hong Kong
   corr               pair-wise correlation
   model              run a localized model of mortality risks
   spatial_model      run a non-local model of mortality risks
   all_models         run all models
   plot_coff          plot distribution of coefficients for each model
   plot_pred          plot mortality predictions by district
   plot_error         plot mortality errors by district
   plot_scale         plot mortality error scaling by district
   plot_vars          plot a correlation heatmap between mortality and some
                       variables of interest for each model
   plot_doi           plot more information for districts of interest
   plot_ego_networks  plot more information for districts of interest
   
optional arguments:
 --maps MAPS          absolute Path to maps (default:
                       ~/Gitlab/asis/maps)
 --models MODELS      absolute Path to save models (default:
                       ~/Gitlab/asis/models)
 --weighted           use a weighted adjacency matrix for the spatial model
                       (default: False)
 -h, --help           show this help message and exit
 -i INDIR, --indir INDIR
                       absolute Path to data directory (default:
                       ~/Gitlab/asis/data)
 -o OUTDIR, --outdir OUTDIR
                       absolute Path to save results (default:
                       ~/Gitlab/asis/results)
````


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.


## Citation
See the following paper for more details:
> Alshaabi, T., Dewhurst, D. R., Bagrow, J. P., Dodds, P. S., & Danforth, C. M. (2020). 
> [The sociospatial factors of death: Analyzing effects of geospatially-distributed variables in a Bayesian mortality model for Hong Kong](https://arxiv.org/abs/2006.08527). 
> *arXiv preprint arXiv:2006.08527*.
>

## License
Licensed under the [MIT License](https://choosealicense.com/licenses/mit/)