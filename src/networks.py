
import networkx as nx
from operator import itemgetter
from matplotlib.patches import Patch
import matplotlib.colorbar as colorbar
import matplotlib.colors as mcolors
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

import consts

try:
    import pygraphviz
    from networkx.drawing.nx_agraph import graphviz_layout
except ImportError:
    try:
        import pydot
        from networkx.drawing.nx_pydot import graphviz_layout
    except ImportError:
        raise ImportError("This script needs Graphviz and either PyGraphviz or pydot")


def update_weights(net):
    """
    Updates weights for an adjacency matrix
    Args:
        net (nx.Graph): a networkx object of the graph

    Returns (np.array):
        a weighted adjacency matrix
    """
    adj = np.zeros((net.number_of_nodes(), net.number_of_nodes()))
    for source in net.nodes():
        for target in net.nodes():
            path = nx.shortest_path(net, source=source, target=target)
            adj[source-1, target-1] = 1/np.exp(len(path)-1)
    np.fill_diagonal(adj, 0)
    return adj


def get_network(adj):
    """
    Create network from adjacency matrix
    Args:
        adj: a numpy array

    Returns (nx.Graph):
        a network object
    """
    G = nx.from_numpy_matrix(adj)
    nx.set_node_attributes(G, consts.id_to_district, 'label')
    mapping = {i: i+1 for i in range(18)}
    G = nx.relabel_nodes(G, mapping)
    return G


def plot_network(net, wnet, hk_map, savepath):
    """
    Plot Hong Kong network of districts
        - Node Size: degree
    Args:
        G (nx.Graph): a networkx object
        hk_map (geopandas.Dataframe): a geo pandas dataframe to plot the map
        savepath (str): path to save network
    """

    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
    })

    fig = plt.figure(figsize=(12, 14))
    gs = fig.add_gridspec(ncols=2, nrows=2)

    mapax = fig.add_subplot(gs[0, :])
    ax = fig.add_subplot(gs[1, 0])
    wax = fig.add_subplot(gs[1, 1])

    cmap = plt.cm.get_cmap('tab20')
    colors = [cmap(i) for i in range(cmap.N)]
    cmap = mcolors.LinearSegmentedColormap.from_list(None, colors, hk_map.shape[0])

    mapax = hk_map.plot(
        column='OBJECTID',
        ax=mapax,
        alpha=0.75,
        edgecolor='k',
        cmap=cmap,
        legend=False,
    )

    #hk_map.apply(lambda x: mapax.annotate(s=x.OBJECTID, xy=x.geometry.centroid.coords[0], ha='center'), axis=1)

    mapax.spines['right'].set_visible(False)
    mapax.spines['top'].set_visible(False)
    mapax.spines['left'].set_visible(False)
    mapax.spines['bottom'].set_visible(False)
    mapax.set_xticks([])
    mapax.set_yticks([])

    (largest_hub, degree) = sorted(net.degree(), key=itemgetter(1))[-1]
    hub_ego = nx.ego_graph(net, largest_hub, radius=3)
    pos = nx.nx_agraph.graphviz_layout(net)
    degrees = dict(hub_ego.degree)

    nx.draw_networkx_nodes(
        hub_ego,
        pos,
        cmap=cmap,
        node_color=[n for n in hub_ego.nodes],
        node_size=[np.exp(v + 1) for v in degrees.values()],
        ax=ax,
        alpha=.95
    )

    nx.draw_networkx_edges(
        hub_ego,
        pos,
        # width=[len(nx.shortest_path(G, source=largest_hub, target=node)) for node in hub_ego.nodes()],
        edge_color='dimgrey',
        ax=ax
    )

    for i, d in enumerate(hk_map.sort_values('OBJECTID').index.values):
        xy = pos[i + 1]
        xytext = (xy[0] + degrees[i + 1] * 6, xy[1] + 15)
        ax.annotate(
            i + 1,
            xy=xy,
            xycoords='data',
            xytext=xytext,
            textcoords='data',
            arrowprops=dict(
                arrowstyle="->",
                connectionstyle="arc3,rad=0.3",
            ),
            fontsize=12,
        )

    (largest_hub, degree) = sorted(wnet.degree(), key=itemgetter(1))[-1]
    hub_ego = nx.ego_graph(wnet, largest_hub, radius=3)
    pos = nx.nx_agraph.graphviz_layout(wnet)

    nx.draw_networkx_nodes(
        hub_ego,
        pos,
        cmap=cmap,
        node_color=[n for n in hub_ego.nodes],
        node_size=1000,
        ax=wax,
        alpha=.95
    )

    nx.draw_networkx_edges(
        hub_ego,
        pos,
        ax=wax,
        width=np.array(list(hub_ego.edges.data('weight', default=0)))[:, 2] * 10,
        edge_color='dimgrey'
    )

    nx.draw_networkx_labels(hub_ego, pos=pos, ax=wax)

    legend = [
        Patch(
            facecolor=cmap(i-1),
            edgecolor=cmap(i-1),
            label=f"{i} - {hk_map.index[hk_map['OBJECTID'] == i].to_list()[0]}"
        )
        for i in hub_ego.nodes
    ]
    legend1 = mapax.legend(
        handles=legend,
        loc='upper left',
        bbox_to_anchor=(.9, .8),
        ncol=1,
        fontsize=10, frameon=False
    )
    mapax.add_artist(legend1)


    mapax.annotate(
        'A', xy=(.2, .85), color='k', weight='bold',
        xycoords="axes fraction", fontsize=16,
    )

    ax.annotate(
        'B', xy=(.35, .95), color='k', weight='bold',
        xycoords="axes fraction", fontsize=16,
    )

    wax.annotate(
        'C', xy=(.2, .95), color='k', weight='bold',
        xycoords="axes fraction", fontsize=16,
    )

    ax.set_axis_off()
    wax.set_axis_off()

    plt.subplots_adjust(top=0.97, right=0.97, wspace=-.05, hspace=.05)
    plt.savefig(f'{savepath}.png', dpi=150, bbox_inches='tight', pad_inches=.25)
    plt.savefig(f'{savepath}.pdf', bbox_inches='tight', pad_inches=.25)


def build_egonet(node, neighbors):
    """
    Build an ego network for each district
    Args:
        node: target node
        neighbors: a dict of features for each neighbor
    Returns:
        a networkx Graph object
    """
    net = nx.Graph()
    for k, d in neighbors.items():
        net.add_node(k, weight=d['mortality'])

        if k != node:
            net.add_edge(
                node, k,
                weight=d['pred'] - d['mortality']
            )
    return net


def plot_egonet(attrs, savepath):
    """
    Build plot ego network for each district
    Args:
        attrs: a dict of node attributes
        savepath: path to save network
    """

    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
    })

    fig, axes = plt.subplots(figsize=(14, 7), ncols=6, nrows=3)

    cbarax = inset_axes(
        axes[0, 4],
        width="300%",
        height="15%",
        bbox_to_anchor=(0, .6, 1, 1),
        bbox_transform=axes[0, 4].transAxes,
    )

    vmin = -1.5
    vmax = 1.5
    step = .5
    bounds = np.arange(vmin, vmax+step, step=step)
    xlabels = [f'{round(x, 2)}' for x in bounds]
    cmap = plt.cm.get_cmap('coolwarm')

    axes = axes.flatten()
    for ax, target in zip(axes, attrs):
        ax.set_title(target, fontsize=12)

        net = build_egonet(target, attrs[target])
        pos = nx.circular_layout(net.nodes())
        pos[target] = np.array([0, 0])

        wgts = [w for n, w in net.nodes(data='weight')]
        ewgts = [net[u][v]['weight'] for u, v in net.edges()]

        nx.draw_networkx_nodes(
            net,
            pos,
            nodelist=[target],
            node_shape='s',
            node_size=250,
            node_color='darkgrey',
            ax=ax
        )
        nx.draw_networkx_nodes(
            net,
            pos,
            node_size=150,
            node_color='k',
            ax=ax
        )
        nx.draw_networkx_nodes(
            net,
            pos,
            node_size=100,
            vmin=vmin,
            vmax=vmax,
            cmap=cmap,
            node_color=wgts,
            cbarax=cbarax,
            ax=ax
        )

        nx.draw_networkx_edges(
            net,
            pos,
            width=5,
            edge_cmap=cmap,
            edge_vmin=vmin,
            edge_vmax=vmax,
            edge_color=ewgts,
            cbarax=cbarax,
            ax=ax,
            arrows=False
        )
        ax.set_axis_off()

        '''
        for i, d in enumerate(pos):
            xy = pos[d]
            xytext = (xy[0]-.001, xy[1]+.025)
            ax.annotate(
                d,
                xy=xy,
                xycoords='data',
                xytext=xytext,
                textcoords='data',
                fontsize=5,
                color='dimgrey'
            )
        '''

    sm = plt.cm.ScalarMappable(
        cmap=cmap,
        norm=plt.Normalize(vmin=vmin, vmax=vmax)
    )
    sm.set_array([])

    plt.colorbar(
        sm,
        cax=cbarax,
        ticks=bounds,
        orientation='horizontal',
    )

    cbarax.set_xticklabels(xlabels)
    cbarax.xaxis.set_ticks_position('bottom')
    cbarax.yaxis.set_label_position('left')
    cbarax.set_ylabel(r'Standard score = $\dfrac{X - \mu}{\sigma}$', rotation=0, labelpad=100, y=-.5)

    plt.savefig(f'{savepath}.png', dpi=200, bbox_inches='tight', pad_inches=.25)
    plt.savefig(f'{savepath}.pdf', bbox_inches='tight', pad_inches=.25)
    plt.close()
    print(savepath)
