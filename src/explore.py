
import sys
import time
from pathlib import Path

import cli
import vis
import heatmaps
import utils


def parse_args(args):
    parser = cli.parser()

    # optional subparsers
    subparsers = parser.add_subparsers(help='Arguments for specific action.', dest='dtype')
    subparsers.required = True

    subparsers.add_parser(
        'all',
        help='regenerate all figures'
    )

    subparsers.add_parser(
        'missing',
        help='plot a heatmap of missing data'
    )

    subparsers.add_parser(
        'monthly_deaths',
        help='plot monthly death records'
    )

    subparsers.add_parser(
        'age_dist',
        help='plot age distribution'
    )

    subparsers.add_parser(
        'tpu_table',
        help='plot deaths by TPU'
    )

    subparsers.add_parser(
        'tpu',
        help='plot features by geotags'
    )

    subparsers.add_parser(
        'district',
        help='plot features by districts'
    )

    subparsers.add_parser(
        'residency',
        help='plot length of staying in Hong Kong'
    )

    subparsers.add_parser(
        'nationality',
        help='plot average age by country of previous residence'
    )

    subparsers.add_parser(
        'occupation',
        help='plot average age by occupation'
    )

    subparsers.add_parser(
        'population',
        help='plot population stats by district'
    )

    subparsers.add_parser(
        'attrs',
        help='plot population stats by district'
    )

    subparsers.add_parser(
        'household',
        help='plot household stats by district'
    )

    parser.add_argument(
        '-i', '--indir',
        default=Path('.').resolve().parent/'data',
        help='absolute Path to data directory'
    )

    parser.add_argument(
        '-o', '--outdir',
        default=Path('.').resolve().parent/'results',
        help='absolute Path to save results'
    )

    parser.add_argument(
        '-m', '--maps',
        default=Path('.').resolve().parent/'maps',
        help='absolute Path to maps'
    )

    return parser.parse_args(args)


def main(args=None):
    timeit = time.time()

    if args is None:
        args = sys.argv[1:]

    args = parse_args(args)
    Path(args.outdir).mkdir(parents=True, exist_ok=True)

    mma = utils.get_mma_records(args.indir/'mma_9518.csv')
    eco = utils.get_census_records(args.indir/'districts_economic_0616.csv', index_col=[0, 1, 2])
    hou = utils.get_census_records(args.indir/'districts_household_0616.csv', index_col=[0, 1])
    deaths = utils.get_death_records(args.indir/'rdeaths_9517.csv.gz')

    if args.dtype == 'all':
        heatmaps.plot_missing_data(deaths, f'{args.outdir}/deaths_missing_data')
        vis.plot_monthly_deaths(deaths, f'{args.outdir}/monthly_deaths')
        heatmaps.plot_tpu_table(deaths, f'{args.outdir}/tpu_table_age', metric='age')
        heatmaps.plot_tpu_table(deaths, f'{args.outdir}/tpu_table_annual_deaths', metric='deaths')
        vis.plot_residency(deaths, f'{args.outdir}/residency')
        vis.plot_nationality(deaths, f'{args.outdir}/nationality')
        vis.plot_occupation(deaths, f'{args.outdir}/occupation')
        heatmaps.plot_tpu_features(deaths, f'{args.outdir}/tpus')
        heatmaps.plot_district_features(deaths, f'{args.outdir}/districts')
        vis.plot_age_dist(deaths, f'{args.outdir}/age_dist')
        vis.plot_mma_age_dist(mma, f'{args.outdir}/mma_age_dist')
        vis.plot_population(eco, f'{args.outdir}/population')
        vis.plot_household(hou, f'{args.outdir}/household')

    elif args.dtype == 'missing':
        heatmaps.plot_missing_data(deaths, f'{args.outdir}/deaths_missing_data')

    elif args.dtype == 'monthly_deaths':
        vis.plot_monthly_deaths(deaths, f'{args.outdir}/monthly_deaths')

    elif args.dtype == 'age_dist':
        vis.plot_age_dist(deaths, f'{args.outdir}/age_dist')
        vis.plot_mma_age_dist(mma, f'{args.outdir}/mma_age_dist')

    elif args.dtype == 'tpu_table':
        heatmaps.plot_tpu_table(deaths, f'{args.outdir}/tpu_table_age', metric='age')
        heatmaps.plot_tpu_table(deaths, f'{args.outdir}/tpu_table_annual_deaths', metric='deaths')

    elif args.dtype == 'residency':
        vis.plot_residency(deaths, f'{args.outdir}/residency')

    elif args.dtype == 'nationality':
        vis.plot_nationality(deaths, f'{args.outdir}/nationality')

    elif args.dtype == 'occupation':
        vis.plot_occupation(deaths, f'{args.outdir}/occupation')

    elif args.dtype == 'tpu':
        heatmaps.plot_tpu_features(deaths, f'{args.outdir}/tpus')

    elif args.dtype == 'district':
        heatmaps.plot_district_features(deaths, f'{args.outdir}/districts')

    elif args.dtype == 'population':
        vis.plot_population(eco, f'{args.outdir}/population')

    elif args.dtype == 'household':
        vis.plot_household(hou, f'{args.outdir}/household')

    elif args.dtype == 'attrs':
        vis.plot_district_attrs(deaths, mma, eco, hou, f'{args.outdir}/attrs')

    else:
        print('Error: unknown action!')

    print(f'Total time elapsed: {time.time() - timeit:.2f} sec.')


if __name__ == "__main__":
    main()
