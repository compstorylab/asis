import re
import pandas as pd
import numpy as np
from datetime import datetime
from pprint import pprint
from functools import partial
from pathlib import Path
import io
import csv
from docx import Document

import warnings
warnings.filterwarnings("ignore")


def decode_age(v):
    """Calculate age (years) for a given individual

    Args:
        v (str): a string starting with either [Y, M, D, X] followed by two or three digits

    Returns: (float)
        age in years or nan

    """
    if v[0] == 'Y':
        try:
            return int(v[1:])
        except ValueError:
            return np.nan
    elif v[0] == 'M':
        try:
            return int(v[1:])/12
        except ValueError:
            return np.nan
    elif v[0] == 'D':
        try:
            return int(v[1:])/365
        except ValueError:
            return np.nan
    else:
        return np.nan


def decode_date(v):
    """Convert given date ('DD-MM-YYYY') to datetime object

    Args:
        v (str): encoded date string

    Returns: (datetime)
        a datetime object or a nan

    """
    try:
        return datetime.strptime(v, '%d-%m-%Y')
    except ValueError:
        try:
            return datetime.strptime(v[3:], '%m-%Y')
        except ValueError:
            try:
                return datetime.strptime(v[6:], '%Y')
            except ValueError:
                return np.nan


def decode_occupation(v):
    """Convert given code to its corresponding occupation

    Args:
        v (str): a sting encoded number

    Returns: (str)
        corresponding occupation
    """
    options = {
        '0': 'Armed forces',
        '1': 'Managers and administrators',
        '2': 'Professionals',
        '3': 'Associate professionals',
        '4': 'Clerks',
        '5': 'Service workers and shop sales workers',
        '6': 'Skilled agricultural and fishery workers',
        '7': 'Craft and related workers',
        '8': 'Plant and machine operators and assemblers',
        '9': 'Elementary occupations',
        'E': 'Economically inactive'
    }
    try:
        return options[v]
    except KeyError:
        return np.nan


def decode_marital_status(v):
    """Convert given code to its corresponding marital status

    Args:
        v (str): a sting encoded number

    Returns: (str)
        corresponding marital status
    """
    options = ['Single', 'Married', 'Widowed', 'Divorced/Legally Separated']
    try:
        return options[int(v)]
    except Exception:
        return np.nan


def decode_place_of_occurrence(v):
    """Convert given code to its corresponding place of occurrence

    Args:
        v (str): a sting encoded number

    Returns: (str)
        corresponding place of occurrence
    """
    options = [
        'Home', 'Residential institution', 'School, other institution and public administrative area',
        'Sports and athletics area', 'Street and highway', 'Trade and service area',
        'Industrial and construction area', 'Farm', 'Other specified places'
    ]
    try:
        return options[int(v)]
    except Exception:
        return np.nan


def digitize(v):
    """Convert given value to a numerical value

    Args:
        v (str): a string encoded number

    Returns: (int)
        an integer
    """
    try:
        return int(v)
    except ValueError:
        return np.nan


def decode_country(v):
    """Convert given code to its corresponding country

    Args:
        v (str): a string encoded number

    Returns: (str)
        name of the country
    """
    options = {
        '01': 'The mainland of China',
        '02': 'Macao',
        '03': 'Taiwan',
        '04': 'Indonesia',
        '05': 'Malaysia',
        '06': 'Singapore',
        '07': 'Philippines',
        '08': 'Thailand',
        '09': 'India, Pakistan, Bangladesh and SriLanka',
        '11': 'Hong Kong',
        '20': 'Vietnam',
        '21': 'Brunei, Laos and Cambodia',
        '22': 'Japan',
        '23': 'Korea',
        '24': 'Other Asian Countries',
        '31': 'Australia',
        '32': 'New Zealand',
        '41': 'United Kingdom',
        '42': 'France',
        '43': 'Germany',
        '44': 'Other European Countries',
        '51': 'Canada',
        '52': 'USA',
        '53': 'Other American Countries',
        '90': 'Other Countries',
    }

    try:
        return options[v]
    except KeyError:
        return np.nan


def district_encoder():
    """Create a hashtable to map TPUs to districts

    Returns: (dict)
        a dictionary of districts (districts-name: [TPUs])
    """
    def formater(s):
        inds = []
        for i in s.split(','):
            if '-' in i:
                bnds = i.split('-')
                start = int(bnds[0])
                end = int(bnds[1]) + 1
                inds.extend(range(start, end))
            else:
                inds.append(int(i))
        return inds

    districts = {
        'Central & Western': formater('111-116, 121-124, 141-143, 181, 182'),
        'Wan Chai': formater('131-135, 140, 144-147, 149, 183, 184, 190'),
        'Eastern': formater('148, 151-158, 161-167'),
        'Southern': formater('171-176, 191-198'),
        'Yau Tsim Mong': formater('211, 212, 214-217, 220-222, 225-229, 251-254, 256'),
        'Sham Shui Po': formater('255, 260-269'),
        'Kowloon City': formater('213, 231-237, 241-247, 271, 272, 285, 286'),
        'Wong Tai Sin': formater('281-284, 287-289'),
        'Kwun Tong': formater('280, 290-295, 297, 298'),
        'Kwai Tsing': formater('320, 326-329, 350, 351'),
        'Tsuen Wan': formater('310, 321-325, 331-336, 340, 731, 973-975'),
        'Tuen Mun': formater('411, 413-416, 421-428, 431-434, 441, 442'),
        'Yuen Long': formater('412, 510-519, 521-529, 531-533, 541-544, 610'),
        'North': formater('545-548, 620-629, 632, 634, 641, 642, 651-653, 711, 712'),
        'Tai Po': formater('631, 633, 720-729, 741-744, 751'),
        'Sha Tin': formater('732, 733, 753-759, 761, 762'),
        'Sai Kung': formater('296, 811-815, 820-829, 831-839'),
        'Island': formater('911-913, 920, 931-934, 941-944, 950, 951, 961-963, 971, 972, 976'),
        'Vietnamese migrants': [61],
        'Illegal immigrants': [74],
        'Transients': [84],
        'Marine': [91],
        'Military camp': [92]
    }

    return districts


def decode_tpu(v, districts):
    """Convert given TPU code to its corresponding district

    Args:
        v (str): a string encoded number
        districts (dict): a dict of available districts

    Returns: (str)
        name of the district
    """
    for dd, ll in districts.items():
        try:
            if int(v) in ll:
                return dd
        except ValueError:
            return np.nan


def tpu2ppu(v):
    """Convert TPU to PPU code

    Args:
        v (str): TPU code

    Returns: (int)
        PPU code
    """
    try:
        return int(str(v)[:1])
    except ValueError:
        return np.nan


def doc2df(filename, tab_id=None, **kwargs):
    """Parse a table from (.doc) file into a DataFrame

    Args:
        filename (str): file name of a Word Document
        tab_id (int): parse a single table with the index: [tab_id] (counting from 0).
            When [None] - return a list of DataFrames (parse all tables)
        **kwargs: arguments to pass to `pd.read_csv()` function

    Returns: (pd.DataFrame)
        a single DataFrame if tab_id != None or a list of DataFrames otherwise
    """
    def tbl(tab, **kwargs):
        vf = io.StringIO()
        writer = csv.writer(vf)
        for row in tab.rows:
            writer.writerow(cell.text for cell in row.cells)
        vf.seek(0)
        return pd.read_csv(vf, **kwargs)

    doc = Document(filename)
    if tab_id is None:
        return [tbl(tab, **kwargs) for tab in doc.tables]
    else:
        try:
            return tbl(doc.tables[tab_id], **kwargs)
        except IndexError:
            print('Error: specified [tab_id]: {}  does not exist.'.format(tab_id))
            raise


def cod_encoder(docpath=Path('../data/HKData/docs/')):
    """Create a hashtable for causes of deaths

    Args:
        docpath (pathlib.Path): path to docs directory

    Returns: (dict)
        a dictionary (code: causes of death)
    """
    icd9 = doc2df(docpath/'icd9des.docx', header=0, names=['code', 'name'])
    icd9 = pd.concat(icd9, ignore_index=True, sort=False)
    icd9['code'] = icd9['code'].astype('str').str.zfill(3)

    icd10 = pd.read_excel(docpath/'icd10updates.xls', header=6, names=['code', 'name']).dropna()
    icd10['code'] = icd10['code'].astype('str').str.replace(".", "")
    icd10['code'] = icd10['code'].astype('str').str.replace("*", "")
    icd10['code'] = icd10['code'].astype('str').str.replace("+", "")

    cods = pd.concat([icd9, icd10], ignore_index=True, sort=False)
    cods = cods.drop_duplicates(subset='code')
    cods = cods.set_index('code')

    return cods.to_dict('index')


def decode_cod(v, cats):
    """Convert a given code to its corresponding causes of death

    Args:
        v (str): a string encoded number
        cats (dict): a dict of all encoded causes of deaths

    Returns (str):
        a string of causes of death
    """
    try:
        return str(cats[v.replace(" ", "")]['name'])
    except KeyError:
        return np.nan


def parser_KDeath9509():
    """A very sketchy regex to parse out HK Known-Deaths dataset [1995-2009]

    Returns (re.pattern):
        a compiled regex
    """
    return re.compile(
        r"([Y|M|D|X][\d|X]{2,3})"   # age
        +"([M|F|U])"                # sex
        +"([\d|X|\s]{2})-"          # day
        +"([\d|X|\s]{2})-"          # month
        +"([\d|X|\s]{4})"           # year
        +"([\w\d|X|\s]{4})"         # cause_of_death
        +"([\w\d|X|\s]{4})"         # external_cause_of_death
        +"([\d|E|X]{1})"            # occupation
        +"([\d|X]{3})"              # place_of_residence (TPU)
        +"([\d|X]{1})"              # marital_status
        +"([\d|X|\s]{2})"           # length_of_stay_in_hong_kong
        +"([\d|X|\s]{2})",          # country_of_previous_residence
        re.ASCII
    )


def parser_KDeath1017():
    """Yet another sketchy regex to parse out HK Known-Deaths dataset [2010-2017]

    Returns (re.pattern):
        a compiled regex
    """
    return re.compile(
        r"([Y|M|D|X][\d|X]{2,3})"   # age
        +"([M|F|U])"                # sex
        +"([\d|X|\s]{2})-"          # day
        +"([\d|X|\s]{2})-"          # month
        +"([\d|X|\s]{4})"           # year
        +"([\w\d|X|\s]{4})"         # cause_of_death
        +"([\w\d|X|\s]{4})"         # external_cause_of_death
        +"([\d|\s]{1})"             # place of occurrence
        +"([\d|E|X]{1})"            # occupation
        +"([\d|X]{3})"              # place_of_residence (TPU)
        +"([\d|X]{1})"              # marital_status
        +"([\d|X|\s]{2})"           # length_of_stay_in_hong_kong
        +"([\d|X|\s]{2})",          # country_of_previous_residence
        re.ASCII
    )


def aggregate_datasets(files, scheme=0):
    """Aggregate longitudinal datasets

    Args:
        files (list): a list of files (.dat) to combine
        scheme (str): string format to use

    Returns: (pd.DataFrame)
        aggregated dataframe
    """
    cols0 = [
        'age', 'sex', 'day', 'month', 'year', 'cause_of_death', 'external_cause_of_death',
        'occupation', 'tpu', 'marital_status', 'length_of_stay_in_hongkong', 'country_of_previous_residence',
    ]

    cols1 = [
        'age', 'sex', 'day', 'month', 'year', 'cause_of_death', 'external_cause_of_death', 'place_of_occurrence',
        'occupation', 'tpu', 'marital_status', 'length_of_stay_in_hongkong', 'country_of_previous_residence',
    ]

    dataframes = []
    for f in files:
        with open(f, 'r') as file:
            data = []
            for line in file:
                try:
                    if scheme == 0:
                        parser = parser_KDeath9509()
                        #print(line)
                        #pprint(dict(zip(cols0, parser.findall(line[:32])[0])))
                        #print('-'*50)
                        data.append(dict(zip(cols0, parser.findall(line[:32])[0])))
                    else:
                        parser = parser_KDeath1017()
                        data.append(dict(zip(cols1, parser.findall(line[:33])[0])))
                except IndexError:
                    print(f'Warning: unknown format [{line}]')

        dataframes.append(pd.DataFrame(data))

    return pd.concat(dataframes, join='outer', ignore_index=True, verify_integrity=True)


def decode_dataset(df):
    """Clean up given dataframe

    Args:
        df (pd.DataFrame): a dataframe of raw data

    Returns: (pd.DataFrame)
        a decoded dataframe
    """
    print('Decoding dataset...')
    districts = district_encoder()
    cats = cod_encoder()

    districtor = partial(decode_tpu, districts=districts)
    cod = partial(decode_cod, cats=cats)

    df['age'] = df['age'].apply(decode_age)
    df['district'] = df['tpu'].apply(districtor)
    df['day'] = df['day'].apply(lambda x: int(x) if x != 'XX' else np.nan)
    df['month'] = df['month'].apply(lambda x: int(x) if x != 'XX' else np.nan)
    df['year'] = df['year'].apply(lambda x: int(x) if x != 'XXXX' else np.nan)
    df['occupation'] = df['occupation'].apply(decode_occupation)
    df['marital_status'] = df['marital_status'].apply(decode_marital_status)
    df['length_of_stay_in_hongkong'] = df['length_of_stay_in_hongkong'].apply(digitize)
    df['length_of_stay_in_hongkong'].loc[df['length_of_stay_in_hongkong'] == 90] = df['age']
    df['country_of_previous_residence'] = df['country_of_previous_residence'].apply(decode_country)
    df['cause_of_death'] = df['cause_of_death'].apply(cod)
    df['external_cause_of_death'] = df['external_cause_of_death'].apply(cod)
    df['place_of_occurrence'] = df['place_of_occurrence'].apply(decode_place_of_occurrence)
    return df.set_index(['year', 'month', 'day'])


def builder(path, savepath):
    """A utility function to build Hong Kong Known Deaths dataset [1995-2017]

    Args:
        path (pathlib.Path): path to raw (.dat) files
        savepath (pathlib.Path): path to save new dataset

    Returns: (pd.DataFrame)
        a decoded dataframe
    """
    files = sorted(path.glob('*[0|9][0-9].dat'))
    pprint(f'Processing: {[p.stem for p in files]}')
    df9509 = aggregate_datasets(files, scheme=0)

    files = sorted(path.glob('*1[0-9].dat'))
    pprint(f'Processing: {[p.stem for p in files]}')
    df1017 = aggregate_datasets(files, scheme=1)

    df = pd.concat([df9509, df1017], ignore_index=True, sort=False)
    print(df.columns.values)
    df = decode_dataset(df)
    df.sort_values(by='year', inplace=True, ascending=False)
    df.to_csv(savepath)
    return df
