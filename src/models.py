import consts
import joblib
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as st
import pandas as pd
import pyro
import pyro.distributions as dist
import torch
import utils
import networks
from matplotlib.patches import Patch
from tqdm import trange
import seaborn as sns
from textwrap import wrap
import matplotlib.colors as mcolors
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import matplotlib.colorbar as colorbar
import matplotlib.patches as patches


def prep_dataframe(crosstab_households, train_years, variables, normalize=True, log_odds_ratio=False):
    """
    Normalize object to reduce potential confusion

    Args:
        crosstab_households: (pd.DataFrame) household temporal data
        train_years: (list) years to use for training
        variables: (list) design variables

    Returns: (pd.DataFrame)
        a normalized dataframe of the variables

    """
    crosstab_households.index = pd.MultiIndex.from_tuples(
        zip(
            crosstab_households.year,
            crosstab_households.district
        )
    )
    crosstab_households = crosstab_households[[v for v in variables if v not in ['year', 'district']]]

    # linear model for normalized log odds ratio instead of normalized mortality
    if log_odds_ratio:
        crosstab_households['mortality'] = crosstab_households['mortality'].apply(lambda x: np.log(x / (1. - x)))

    mean_ = crosstab_households.loc[
        crosstab_households.index.isin(train_years, level=0)
    ].mean()

    std_ = crosstab_households.loc[
        crosstab_households.index.isin(train_years, level=0)
    ].std()

    if normalize:
        return (crosstab_households - mean_) / std_
    else:
        return crosstab_households


def plot_loss(df, savepath):
    """
    Plot loss over number of iterations for each model
    Args:
        df: (pd.DataFrame) a dataframe of loss values at each time step
        savepath: (str) path to save plot
    """
    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12
    })

    legend = {}
    cats = ['base', 'wealth', 'all']
    colors = dict(zip(
        ['model', 'spatial_model', 'weighted_spatial_model'],
        consts.group_colors
    ))
    fig, axes = plt.subplots(figsize=(10, 4), ncols=len(cats), nrows=1, sharex=True)

    for ax, cat in zip(axes.flatten(), cats):
        for model in df.columns[df.columns.str.endswith(cat)]:
            label, color = '', 'k'
            for k, c in colors.items():
                if model.startswith(k):
                    label, color = k, c

            ax.plot(
                df[model],
                marker='o',
                ms=3,
                color=color,
                mfc=color,
                mec=color,
                alpha=.1,
                lw=0,
            )

            ax.plot(
                df[model].rolling(df.shape[0]//10, center=True).mean(),
                color=color,
                lw=2,
            )

            ax.set_title(cat.capitalize())
            ax.set_xlim(df.index[0], df.index[-1])
            ax.set_ylim(0, 400)
            ax.set_xlabel('Time step $t$')

            legend[label.replace('_', ' ').capitalize()] = Patch(
                facecolor=color,
                edgecolor=color,
                label=label.replace('_', ' ').capitalize()
            )

    axes[0].legend(
        handles=legend.values(),
        loc='upper left',
        ncol=1,
        fontsize=10,
        frameon=False
    )

    axes[0].set_ylabel('Loss')

    plt.tight_layout()
    plt.savefig(f'{savepath}.png', dpi=200, bbox_inches='tight', pad_inches=.25)


def base_regression_model(design_tensor, response_variable=None):
    """
    A base nonstationary linear model.

    The model takes the form

    y_t = X_t b_t + \sigma_t w_t
    b_t = b_{t-1} + s e_t
    \log \sigma_t = \log \sigma_{t-1} + v xi_t

    Here w_t, e_t, and xi_t are independent Gaussian noise,
    and s and v are independent positive rvs.

    Args:
        design_tensor: (torch.tensor) tensor of shape (T, N, p)
        response_variable: (torch.tensor) tensor of shape (T, N, 1)
    """
    # do NOT include column of ones when passing in
    p = design_tensor.shape[-1] + 1
    N = design_tensor.shape[-2]
    T = design_tensor.shape[0]

    # hyper covariances
    # uniform distributions over correlation matrices
    s_corr_chol = pyro.sample(
        's_corr_chol',
        dist.LKJCorrCholesky(p, eta=torch.ones(1))
    )
    # making covariance matrices
    # sample the variances and then deterministic transform
    s_sd = pyro.sample(
        's_sd',
        dist.LogNormal(0., 1.).expand((p,))
    )
    v_sd = pyro.sample(
        'v_sd',
        dist.LogNormal(0., 1.)
    )

    s_covariance = torch.diag(s_sd).matmul(s_corr_chol.matmul(s_corr_chol.T)).matmul(torch.diag(s_sd))

    # prior over regression coefficients
    beta_0 = pyro.sample(
        'beta_0',
        dist.MultivariateNormal(torch.zeros(p), covariance_matrix=s_covariance)
    )
    # mean of increments is just diagonal normal
    mu_beta = pyro.sample(
        'mu_beta',
        dist.MultivariateNormal(torch.zeros(p), torch.eye(p))
    )
    beta_noise = pyro.sample(
        'beta_noise',
        dist.MultivariateNormal(mu_beta, covariance_matrix=s_covariance).expand((T,))
    )
    beta = pyro.deterministic(
        'beta',
        beta_0 + beta_noise.cumsum(dim=0)
    )

    # do the same thing as above but for log_sigma
    log_sigma_0 = pyro.sample(
        'log_sigma_0',
        dist.Normal(0, v_sd)
    )
    mu_log_sigma = pyro.sample(
        'mu_log_sigma',
        dist.Normal(0., 1.)
    )
    log_sigma_noise = pyro.sample(
        'log_sigma_noise',
        dist.Normal(mu_log_sigma, v_sd).expand((T,))
    )
    log_sigma = pyro.deterministic(
        'log_sigma',
        log_sigma_0 + log_sigma_noise.cumsum(dim=0)
    )

    sigma = log_sigma.mul(0.5).exp().clamp(min=1e-5, max=1e5)

    # now observe a linear model at each timestep
    means = []
    for t in pyro.poutine.markov(range(T)):
        with pyro.plate(f"plate_{t}", N):
            mean_t = pyro.deterministic(
                f'mean_{t}',
                torch.matmul(design_tensor[t], beta[t, 1:]) + beta[t, 0]
            )
            means.append(mean_t)
            obs_t = pyro.sample(
                f"y_{t}",
                dist.Normal(mean_t, sigma[t]),
                obs=response_variable[t] if response_variable is not None else None
            )
    means = torch.stack(means, dim=0)

    return means


def network_regression_model(design_tensor, adj, response_variable=None):
    """
    A nonlocal (network) nonstationary linear model.

    The model takes the form

    y_t = X_t b_t + reduce_func(A \otimes X_t) gamma_t + \sigma_t w_t
    b_t = b_{t-1} + s e_t
    gamma_t = gamma_{t-1} + q u_t
    \log \sigma_t = \log \sigma_{t-1} + v xi_t

    Here w_t, e_t, and xi_t are independent Gaussian noise,
    and s and v are independent positive rvs.
    adj is the adjacency matrix of nodes and c_t
    is either a vector or matrix of spatial factors.

    Args:
        design_tensor: (torch.tensor) tensor of shape (T, N, p)
        adj: (np.array) adjacency matrix
        response_variable: (torch.tensor) tensor of shape (T, N, 1)
    """
    # do NOT include column of ones when passing in
    p = design_tensor.shape[-1] + 1
    N = design_tensor.shape[-2]
    T = design_tensor.shape[0]

    # make the spatial tensor
    x_oplus_a = torch.einsum(
        'tij, ik -> tkij',
        design_tensor,
        adj
    )

    # Experimental: (average effect of the neighborhood)
    # you can either change this here or change the weights in the adjacency matrix A
    # shape is now (T, N, p - 1) again down from (T, N, N, p - 1)
    reduced_x_oplus_a = x_oplus_a.mean(dim=1)

    # hyper covariances
    # uniform distributions over correlation matrices
    s_corr_chol = pyro.sample(
        's_corr_chol',
        dist.LKJCorrCholesky(p, eta=torch.ones(1))
    )
    q_corr_chol = pyro.sample(
        'q_corr_chol',
        dist.LKJCorrCholesky(p - 1, eta=torch.ones(1))  # no constant here
    )

    # making covariance matrices
    # sample the variances and then deterministic transform
    s_sd = pyro.sample(
        's_sd',
        dist.LogNormal(0., 1.).expand((p,))
    )
    q_sd = pyro.sample(
        'q_sd',
        dist.LogNormal(0., 1.).expand((p - 1,))
    )
    v_sd = pyro.sample(
        'v_sd',
        dist.LogNormal(0., 1.)
    )

    s_covariance = torch.diag(s_sd).matmul(s_corr_chol.matmul(s_corr_chol.T)).matmul(torch.diag(s_sd))
    q_covariance = torch.diag(q_sd).matmul(q_corr_chol.matmul(q_corr_chol.T)).matmul(torch.diag(q_sd))

    # prior over regression coefficients
    beta_0 = pyro.sample(
        'beta_0',
        dist.MultivariateNormal(torch.zeros(p), covariance_matrix=s_covariance)
    )
    # mean of increments is just diagonal normal
    mu_beta = pyro.sample(
        'mu_beta',
        dist.MultivariateNormal(torch.zeros(p), torch.eye(p))
    )
    beta_noise = pyro.sample(
        'beta_noise',
        dist.MultivariateNormal(mu_beta, covariance_matrix=s_covariance).expand((T,))
    )
    beta = pyro.deterministic(
        'beta',
        beta_0 + beta_noise.cumsum(dim=0)
    )

    # prior over regression coefficients
    gamma_0 = pyro.sample(
        'gamma_0',
        dist.MultivariateNormal(torch.zeros(p - 1), covariance_matrix=q_covariance)
    )
    # mean of increments is just diagonal normal
    mu_gamma = pyro.sample(
        'mu_gamma',
        dist.MultivariateNormal(torch.zeros(p - 1), torch.eye(p - 1))
    )
    gamma_noise = pyro.sample(
        'gamma_noise',
        dist.MultivariateNormal(mu_gamma, covariance_matrix=q_covariance).expand((T,))
    )
    gamma = pyro.deterministic(
        'gamma',
        gamma_0 + gamma_noise.cumsum(dim=0)
    )

    # do the same thing as above but for log_sigma
    log_sigma_0 = pyro.sample(
        'log_sigma_0',
        dist.Normal(0, v_sd)
    )
    mu_log_sigma = pyro.sample(
        'mu_log_sigma',
        dist.Normal(0., 1.)
    )
    log_sigma_noise = pyro.sample(
        'log_sigma_noise',
        dist.Normal(mu_log_sigma, v_sd).expand((T,))
    )
    log_sigma = pyro.deterministic(
        'log_sigma',
        log_sigma_0 + log_sigma_noise.cumsum(dim=0)
    )

    sigma = log_sigma.mul(0.5).exp().clamp(min=1e-5, max=1e5)

    # now observe a linear model at each timestep
    means = list()
    for t in pyro.poutine.markov(range(T)):
        with pyro.plate(f"plate_{t}", N):
            mean_t = pyro.deterministic(
                f'mean_{t}',
                torch.matmul(
                    design_tensor[t],
                    beta[t, 1:]) + beta[t, 0] + torch.matmul(
                    reduced_x_oplus_a[t],
                    gamma[t]
                )
            )
            means.append(mean_t)
            obs_t = pyro.sample(
                f"y_{t}",
                dist.Normal(mean_t, sigma[t]),
                obs=response_variable[t] if response_variable is not None else None
            )
    means = torch.stack(means, dim=0)

    return means


def fit_model(
        df,
        model,
        savepath,
        niter=1000,
        num_samples=1000,
        lr=0.01,
        train_years=('2006', '2011'),
        adjmat=None
):
    """
    Fit model with SVI

    Args:
        df: (pd.DataFrame) household temporal data
        model: (func) model to fit here
        savepath: (pathlib.Path) path to save trained model
        niter:  (int) number of iterations
        num_samples: (int) number of samples
        lr: (float) initial learning rate
        train_years: (list) years to use for training
        variables: (list) design variables
        adjmat: (np.array) adjacency matrix for the spatial model
    """
    T = len(train_years)
    N = len(np.unique(df.index.get_level_values(1)))

    response_variable = torch.tensor(
        df.loc[
            df.index.isin(train_years, level=0),
            df.columns == 'mortality'
        ].values.reshape(T, N),
        dtype=torch.float
    )

    design_tensor = torch.tensor(
        df.loc[
            df.index.isin(train_years, level=0),
            df.columns != 'mortality'
        ].values.reshape(T, N, -1),
        dtype=torch.float
    )

    if adjmat is not None:
        adjmat = torch.tensor(adjmat, dtype=torch.float)

    # fit the model
    guide = pyro.infer.autoguide.AutoLowRankMultivariateNormal(model)
    svi = pyro.infer.SVI(
        model,
        guide,
        pyro.optim.Adam({'lr': lr}),
        loss=pyro.infer.Trace_ELBO()
    )

    pyro.clear_param_store()
    losses = []
    with trange(niter, leave=True) as pbar:
        for i in pbar:
            if adjmat is not None:
                loss = svi.step(
                    design_tensor,
                    adj=adjmat,
                    response_variable=response_variable,
                )
            else:
                loss = svi.step(
                    design_tensor,
                    response_variable=response_variable,
                )

            if i % 10 == 0:
                pbar.set_description(f"Fitting {savepath.stem} (loss = {round(loss, 2)})")
            losses.append(loss)

    predictive = pyro.infer.Predictive(
        model,
        guide=guide,
        num_samples=num_samples,
    )

    if adjmat is not None:
        posterior = predictive(design_tensor, adj=adjmat)
    else:
        posterior = predictive(design_tensor)

    joblib.dump(posterior, f'{savepath}.gz')

    return losses


def plot_model_results(df, model, savepath, coff='beta', ci=.8):
    """
    Plot distribution of coefficients for each model colored by significance
        (blue) negatively correlated with mortality
        (orange) positively correlated with mortality
        (grey) not significantly correlated with mortality
    Args:
        df: (pd.DataFrame) household temporal data
        model: (str) path to model
        savepath: (str) path to save plots
        coff: (str) target coefficient to plot

    """
    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12
    })

    # approximate empirical posterior found through VI
    # these are draws from the approximate posterior
    design_variables = ['constant'] + [x for x in df.columns if x != 'mortality']
    posterior = joblib.load(model)
    p = len(design_variables)
    timesteps = {i: y for i, y in zip(range(3), ['2006', '2011', '2016'])}

    n_subplots = int(np.prod(posterior[coff].shape[-2:]))
    subplot_shape = utils.get_dims(n_subplots)
    fig, axes = plt.subplots(*subplot_shape, figsize=(14, 14), sharey=True, sharex=True)
    axes = axes.flatten()

    beta = posterior[coff].detach().squeeze().view(
        (posterior[coff].shape[0], n_subplots)
    ).T

    for i, (cs, ax) in enumerate(zip(beta, axes)):
        mu = cs.mean(dim=0).item()
        std = cs.std(dim=0).item()
        cil, cih = st.t.interval(ci, cs.shape[0], loc=mu, scale=std)

        # color hists by significance
        if cil > 0.:
            color = 'C1'
        elif cih < 0.:
            color = 'C0'
        else:
            color = 'k'

        sns.distplot(
            cs,
            bins='sqrt',
            color=color,
            kde_kws={"color": "k", "label": "KDE"} if i == 0 else None,
            ax=ax
        )

        ax.grid(True, which="both", axis='both', lw=1, linestyle='-', alpha=.5)
        ax.xaxis.set_tick_params(which='both', labelbottom=True)
        ax.axes.get_yaxis().set_visible(False)

        t = design_variables[i % p]
        ax.set_title(
            f"[{timesteps[(3 * i) // n_subplots]}] "
            f"{t.capitalize() if len(t.split('_')) == 1 else ''.join([i[0] for i in t.split('_')]).upper()}\n"
            f"$E[\\{coff}] = {round(mu, 2)}$"
        )

        ax.axvline(mu, 0, 1, color='C3', ls='--', lw=2, label=f"$E[\\{coff}]$")
        ax.axvline(cil, 0, 1, color='C5', ls='--', lw=2, label=f'{int(ci*100)}% CI')
        ax.axvline(cih, 0, 1, color='C5', ls='--', lw=2)

    for ax in axes[n_subplots:]:
        ax.axis('off')

    axes[0].legend(frameon=False, ncol=1, loc='upper left', bbox_to_anchor=(0, .9))
    sns.despine(left=True, bottom=False)

    plt.tight_layout()
    plt.savefig(f'{savepath}_{coff}.png', dpi=200, bbox_inches='tight', pad_inches=.25)
    plt.savefig(f'{savepath}_{coff}.pdf', bbox_inches='tight', pad_inches=.25)
    plt.close()
    print(f'{savepath}_{coff}')


def plot_model_kde(df, models, savepath, year):
    """
    Plot mortality kernel density estimation by district

    Args:
        df: (pd.DataFrame) household temporal data
        models: (list) list of models
        savepath: (str) path to save plots
        year: (str) target year
    """

    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12
    })

    timesteps = {y: i for y, i in zip(['2006', '2011', '2016'], range(3))}
    fig, axes = plt.subplots(ncols=3, nrows=6, figsize=(12, 14), sharey=True)
    axes = axes.flatten()

    for m in models:
        name = ' '.join(m.stem.split('_')[:-2]).capitalize()
        model = joblib.load(m)
        mean_preds = model[f'mean_{timesteps[year]}'].detach()

        mortality = df.loc[
            df.index.isin([year], level=0),
            df.columns == 'mortality'
        ].reset_index(level=0, drop=True).loc[
            list(consts.id_to_district.values())
        ].values.reshape((-1,))

        for i, (r_i, mu_i, ax, tag) in enumerate(zip(mortality, mean_preds.T, axes, consts.tags)):
            sns.distplot(
                mu_i,
                bins='sqrt',
                color=consts.models_cmap.get(name),
                hist=False,
                kde_kws={
                    "color": consts.models_cmap.get(name),
                    'ls': consts.models_ls.get(name),
                    'lw': 3,
                },
                label=consts.models_codenames.get(name) if i == 0 else None,
                ax=ax
            )
            ax.axvline(r_i, 0, 1, color='r', linestyle=':', lw=3)

            ax.grid(True, which="both", axis='both', lw=1, linestyle='--', alpha=.5)
            ax.xaxis.set_tick_params(which='both', labelbottom=True)
            ax.set_title(consts.id_to_district[i])
            ax.axes.get_yaxis().set_visible(False)

            ax.annotate(
                tag, xy=(0, 1.1), color='k', weight='bold',
                xycoords="axes fraction", fontsize=16,
            )

    for ax in axes[18:]:
        ax.axis('off')

    axes[0].legend(frameon=False, ncol=1, loc='upper left', bbox_to_anchor=(0, 1))
    sns.despine(left=True, bottom=False)

    plt.tight_layout()
    plt.savefig(f'{savepath}.png', dpi=200, bbox_inches='tight', pad_inches=.25)
    plt.savefig(f'{savepath}.pdf', bbox_inches='tight', pad_inches=.25)
    plt.close()
    print(savepath)


def plot_model_dists(df, models, savepath, year, ci=.8, error=False):
    """
    Plot model distributions by district

    Args:
        df: (pd.DataFrame) household temporal data
        models: (list) list of models
        savepath: (str) path to save plots
        year: (str) target year
        ci: (float) CI intervals
        error: (bool) plot error distributions otherwise plot predictions
    """

    def adjacent_values(vals, q1, q3):
        upper_adjacent_value = q3 + (q3 - q1) * 1.5
        upper_adjacent_value = np.clip(upper_adjacent_value, q3, vals[-1])

        lower_adjacent_value = q1 - (q3 - q1) * 1.5
        lower_adjacent_value = np.clip(lower_adjacent_value, vals[0], q1)
        return lower_adjacent_value, upper_adjacent_value

    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12
    })

    timesteps = {y: i for y, i in zip(['2006', '2011', '2016'], range(3))}

    preds = {}
    data = {}
    res = {}
    for m in models:
        name = ' '.join(m.stem.split('_')[:-2]).capitalize()
        model = joblib.load(m)
        mean_preds = model[f'mean_{timesteps[year]}'].detach()

        mortality = df.loc[
            df.index.isin([year], level=0),
            df.columns == 'mortality'
        ].reset_index(level=0, drop=True).loc[
            list(consts.id_to_district.values())
        ].values.reshape((-1,))

        for i, (r_i, mu_i) in enumerate(zip(mortality, mean_preds.T)):
            if error:
                vals = mu_i.numpy() - r_i
            else:
                vals = mu_i.numpy()

            if data.get(consts.models_codenames.get(name)) is not None:
                data[consts.models_codenames.get(name)].extend(list(abs(vals)))
            else:
                data[consts.models_codenames.get(name)] = list(abs(vals))

            if preds.get(consts.id_to_district[i]) is None:
                preds[consts.id_to_district[i]] = {}
                res[consts.id_to_district[i]] = {}

            preds[consts.id_to_district[i]].update({
                'y': r_i,
                consts.models_codenames.get(name): vals
            })

            res[consts.id_to_district[i]].update({
                consts.models_codenames.get(name): np.mean(np.random.choice(vals, 1000))
            })

    fig, axes = plt.subplots(ncols=3, nrows=6, figsize=(12, 14), sharey=True)
    axes = axes.flatten()

    for i, (district, ax, tag) in enumerate(zip(preds, axes, consts.tags)):
        data = [preds[district][m] for m in consts.models_codenames.values()]

        parts = ax.violinplot(
            data,
            showmeans=False,
            showmedians=False,
            showextrema=False
        )

        quartile1, medians, quartile3 = np.percentile(data, [25, 50, 75], axis=1)
        whiskers = np.array([
            adjacent_values(sorted_array, q1, q3)
            for sorted_array, q1, q3 in zip(data, quartile1, quartile3)])
        whiskersMin, whiskersMax = whiskers[:, 0], whiskers[:, 1]

        inds = np.arange(1, len(medians) + 1)
        ax.scatter(inds, medians, marker='o', color='w', s=5, zorder=3)
        ax.vlines(inds, quartile1, quartile3, color='k', linestyle='-', lw=3)
        ax.vlines(inds, whiskersMin, whiskersMax, color='k', linestyle='-', lw=1)

        if error:
            ax.axhline(0, 0, 1, color='r', linestyle='-')
        else:
            ax.axhline(preds[district]['y'], 0, 1, color='r', linestyle='-')

        for pc, m in zip(parts['bodies'], consts.models_codenames.values()):
            cs = preds[district][m]
            mu = np.mean(cs)
            std = np.std(cs)
            cil, cih = st.t.interval(ci, len(cs), loc=mu, scale=std)
            lim = 0 if error else preds[district]['y']

            # color hists by significance
            if cil > lim:
                color = 'C1'
            elif cih < lim:
                color = 'C0'
            else:
                color = 'grey'

            pc.set_facecolor(color)
            pc.set_edgecolor('k')
            pc.set_alpha(.5)

        ax.set_title(district)
        ax.set_xticklabels(consts.models_codenames.values())
        ax.grid(True, which="both", axis='both', lw=1, linestyle='--', alpha=.5)

        if i % 3 == 0:
            if error:
                ax.set_ylabel(r'Signed deviation')
            else:
                ax.set_ylabel('Normalized\nMortality Rate')

        ax.annotate(
            tag, xy=(-.2, 1.1), color='k', weight='bold',
            xycoords="axes fraction", fontsize=16,
        )

    sns.despine(left=False, bottom=False, offset=10, trim=True)

    plt.tight_layout()
    plt.savefig(f'{savepath}.png', dpi=200, bbox_inches='tight', pad_inches=.25)
    plt.savefig(f'{savepath}.pdf', bbox_inches='tight', pad_inches=.25)
    plt.close()
    print(savepath)

    df = pd.DataFrame.from_dict(res, orient='index')
    return df[['Baseline', 'SP', 'WSP']]


def plot_model_scale(df, models, savepath, year):
    """
    Plot model distributions by district

    Args:
        df: (pd.DataFrame) household temporal data
        models: (list) list of models
        savepath: (str) path to save plots
        year: (str) target year
    """

    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 10
    })

    timesteps = {y: i for y, i in zip(['2006', '2011', '2016'], range(3))}

    data = {}
    for m in models:
        name = ' '.join(m.stem.split('_')[:-2]).capitalize()
        model = joblib.load(m)
        mean_preds = model[f'mean_{timesteps[year]}'].detach()

        vars = {}
        for c in df.columns:
            vars[c] = df.loc[
                df.index.isin([year], level=0),
                df.columns == c
            ].reset_index(level=0, drop=True).loc[
                list(consts.id_to_district.values())
            ].values.reshape((-1,))

        for i, mu_i in enumerate(mean_preds.T):
            error = mu_i.mean().item() - vars['mortality'][i]

            if data.get(consts.id_to_district[i]) is None:
                data[consts.id_to_district[i]] = {}

            data[consts.id_to_district[i]].update({
                consts.models_codenames.get(name): error,
            })
            data[consts.id_to_district[i]].update({
                c: vars[c][i] for c in vars
            })

    data = pd.DataFrame(data).T

    fig, axes = plt.subplots(ncols=3, nrows=4, figsize=(10, 10), sharey=True)
    axes = axes.flatten()

    results = []
    for i, (ax, var, tag) in enumerate(zip(axes, consts.voi, consts.tags)):
        for m in consts.models_codenames:

            slope, intercept, r_value, p_value, std_err = st.linregress(
                data[var], data[consts.models_codenames.get(m)]
            )

            results.append({
                'year': year,
                'model': consts.models_codenames.get(m),
                'var': var,
                'slope': slope,
                'r_value': r_value,
                'p_value': p_value,
                'std_err': std_err,
            })

            ax = sns.regplot(
                x=var,
                y=consts.models_codenames.get(m),
                data=data,
                ax=ax,
                color=consts.models_cmap.get(m),
                label=consts.models_codenames.get(m),
                scatter=False,
            )
            sns.scatterplot(
                x=var,
                y=consts.models_codenames.get(m),
                data=data,
                ax=ax,
                color='k',
                alpha=.25,
                s=10,
            )

            ax.set_xlim(-2, 2)
            ax.set_ylim(-2, 2)

        if i == 0:
            ax.legend(frameon=False, ncol=1, loc='upper left', bbox_to_anchor=(0, 1.4))
        else:
            ax.get_legend().remove()

        ax.set_xlabel('\n'.join(wrap(var.replace('_', ' ').capitalize(), 30)))

        if i % 3 == 0:
            ax.set_ylabel('Average\ninference error')
        else:
            ax.set_ylabel('')

        ax.annotate(
            tag, xy=(-.2, 1.2), color='k', weight='bold',
            xycoords="axes fraction", fontsize=16,
        )

    sns.despine(left=False, bottom=False, offset=10, trim=True)

    plt.tight_layout()
    plt.savefig(f'{savepath}.png', dpi=200, bbox_inches='tight', pad_inches=.25)
    plt.close()
    print(savepath)

    return results


def plot_vars(df, savepath):

    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 10
    })

    def gen_cmap(cmap, bounds):
        cmaplist = [cmap(i) for i in range(cmap.N)]
        cmaplist[-1] = (1, 1, 1, 1.0)
        cmap = mcolors.LinearSegmentedColormap.from_list(None, cmaplist, cmap.N)
        norm = mcolors.BoundaryNorm(bounds, cmap.N)
        return cmap, norm

    fig, axes = plt.subplots(ncols=3, nrows=4, figsize=(10, 10))
    axes = axes.flatten()

    vmin, vmax = 0, 1
    bounds = [0, .05, .1, .15, .2, .5, 1]
    cmap, norm = gen_cmap(plt.cm.get_cmap('magma'), bounds)

    cbarax = inset_axes(
        axes[1],
        width="200%",
        height="8%",
        bbox_to_anchor=(.5, .5, 1, 1),
        bbox_transform=axes[1].transAxes,
    )

    for i, (ax, var, tag) in enumerate(zip(axes, consts.voi, consts.tags)):
        d = df[df['var'] == var]
        mat = d.pivot(index='year', columns='model', values='p_value')
        sns.heatmap(
            mat,
            ax=ax,
            cbar_ax=cbarax,
            cbar=True,
            cmap=cmap,
            vmin=vmin,
            vmax=vmax,
            norm=norm,
            linewidths=1,
            linecolor='k',
        )

        ax.annotate(
            tag, xy=(-.2, 1.2), color='k', weight='bold',
            xycoords="axes fraction", fontsize=16,
        )
        ax.set_xlabel('')
        ax.set_ylabel('')
        ax.set_title('\n'.join(wrap(var.replace('_', ' ').capitalize(), 30)))
        ax.set_yticklabels(ax.get_yticklabels(), va='center')

    cb = colorbar.ColorbarBase(
        cbarax,
        cmap=cmap,
        norm=norm,
        spacing='uniform',
        ticks=bounds,
        boundaries=bounds,
        orientation='horizontal',
    )

    cbarax.tick_params(labelsize=12)
    cbarax.xaxis.set_ticks_position('top')
    cbarax.yaxis.set_label_position('left')
    cbarax.set_ylabel(r'$p$-values', rotation=0, labelpad=50, y=0)

    plt.tight_layout()
    plt.savefig(f'{savepath}.png', dpi=200, bbox_inches='tight', pad_inches=.25)
    plt.close()
    print(savepath)


def plot_doi(df, models, net, savepath, year):
    """

    Args:
        df: (pd.DataFrame) household temporal data
        models: (list) list of models
        net: (nx.Graph) networkx graph object
        savepath: (str) path to save plots
        year: (str) target year
    """
    timesteps = {y: i for y, i in zip(['2006', '2011', '2016'], range(3))}
    data = {consts.id_to_district[d]: {} for d in consts.id_to_district}

    wealth = [
        'population',
        'homeless',
        'mobile_residents',
        'unemployment',
        'median_income',
        'single_parents',
        'minorities'
    ]

    for m in models:
        name = ' '.join(m.stem.split('_')[:-2]).capitalize()
        model = joblib.load(m)
        mean_preds = model[f'mean_{timesteps[year]}'].detach().T
        mortality = df.loc[
            df.index.isin([year], level=0),
            df.columns == 'mortality'
        ].reset_index(level=0, drop=True).loc[
            list(consts.id_to_district.values())
        ].values.reshape((-1,))

        for d in consts.id_to_district:
            neighbors = [i-1 for i in net.neighbors(d + 1)]
            data[consts.id_to_district[d]][consts.models_codenames.get(name)] = mean_preds[d].mean().item() - mortality[d]
            data[consts.id_to_district[d]]['mortality'] = mortality[d]
            data[consts.id_to_district[d]]['mortality*'] = np.mean([mortality[n] for n in neighbors])

            for f in wealth:
                w = df.loc[
                    df.index.isin([year], level=0),
                    df.columns == f
                ].reset_index(level=0, drop=True).loc[
                    list(consts.id_to_district.values())
                ].values.reshape((-1,))

                data[consts.id_to_district[d]][f] = w[d]
                data[consts.id_to_district[d]][f'{f}*'] = np.mean([w[n] for n in neighbors])

    order = ['Baseline', 'SP', 'WSP']
    for w in ['mortality']+wealth:
        order.append(w)
        order.append(f'{w}*')

    df = pd.DataFrame(data).loc[order, :].reset_index()
    df['index'] = df['index'].replace(
        {x: '\n'.join(wrap(x.replace('_', ' ').capitalize(), 30)) for x in df['index'][3:]}
    )
    df['Label'] = ~df['index'].str.contains('*', regex=False)
    df['Label'] = df['Label'].replace({True: 'District', False: 'Neighbors'})
    df['index'] = df['index'].str.replace('*', '')

    g = sns.PairGrid(
        df,
        x_vars=consts.doi.values(),
        y_vars=['index'],
    )
    g.fig.set_size_inches(8, 4)
    g.map(
        sns.scatterplot,
        hue=df['Label'],
        style=df['Label'],
        size=df['Label'],
        hue_order=["District", "Neighbors"],
        palette={"District": "k", "Neighbors": 'C1'},
        sizes={"District": 60, "Neighbors": 75},
        x_jitter=0,
        y_jitter=0,
        zorder=3,
    )
    g.set(xlabel="", ylabel="")
    g.add_legend(title="", adjust_subtitles=True, loc='upper left')

    for i, (ax, title, label) in enumerate(zip(g.axes.flat, consts.doi.values(), consts.tags)):
        ax.set(title=title)
        ax.grid(True, which="both", axis='both', lw=1, linestyle='-', zorder=0)
        ax.annotate(
            label, xy=(-.11, 1.), color='k', weight='bold',
            xycoords="axes fraction", fontsize=16,
        )
        ax.set_xlim(-3, 3)
        ax.set_xticks(range(-2, 3))
        ax.axvline(
            0,
            ls='--',
            lw=1.25,
            color='red',
            alpha=.75
        )
        ax.add_patch(patches.Rectangle(
            (-3.5, -.4),
            9, 2.9,
            linewidth=2,
            edgecolor='C0',
            facecolor='none',
            zorder=3,
        ))

    sns.despine(left=True, bottom=True)
    plt.tight_layout()
    plt.savefig(f'{savepath}.png', dpi=200, bbox_inches='tight', pad_inches=.25)
    plt.savefig(f'{savepath}.pdf', bbox_inches='tight', pad_inches=.25)
    plt.close()
    print(savepath)


def plot_ego_networks(df, models, net, savepath, year):
    """
    Plot nonlocal interactions between districts
    Args:
        df: (pd.DataFrame) household temporal data
        models: (list) list of models
        net: (nx.Graph) networkx graph object
        savepath: (str) path to save influence networks
        year: (str) target year
    """
    timesteps = {y: i for y, i in zip(['2006', '2011', '2016'], range(3))}
    wealth = [
        'population',
        'homeless',
        'mobile_residents',
        'unemployment',
        'median_income',
        'single_parents',
        'minorities',
    ]

    for m in models:
        influence = {}
        name = consts.models_codenames.get(' '.join(m.stem.split('_')[:-2]).capitalize())
        model = joblib.load(m)
        mean_preds = model[f'mean_{timesteps[year]}'].detach().T

        mortality = df.loc[
            df.index.isin([year], level=0),
            df.columns == 'mortality'
        ].reset_index(level=0, drop=True).loc[
            list(consts.id_to_district.values())
        ].values.reshape((-1,))

        for d in consts.id_to_district:
            #print(consts.id_to_district[d], mortality[d])

            influence[consts.id_to_district[d]] = {}
            neighbors = [i-1 for i in net.neighbors(d + 1)]

            influence[consts.id_to_district[d]][consts.id_to_district[d]] = {}
            influence[consts.id_to_district[d]][consts.id_to_district[d]]['mortality'] = mortality[d]
            influence[consts.id_to_district[d]][consts.id_to_district[d]]['pred'] = mean_preds[d].mean().item()

            for n in neighbors:
                influence[consts.id_to_district[d]][consts.id_to_district[n]] = {}
                influence[consts.id_to_district[d]][consts.id_to_district[n]]['mortality'] = mortality[n]
                influence[consts.id_to_district[d]][consts.id_to_district[n]]['pred'] = mean_preds[n].mean().item()

            for f in wealth:
                w = df.loc[
                    df.index.isin([year], level=0),
                    df.columns == f
                ].reset_index(level=0, drop=True).loc[
                    list(consts.id_to_district.values())
                ].values.reshape((-1,))

                influence[consts.id_to_district[d]][consts.id_to_district[d]][f] = w[d]
                for n in neighbors:
                    influence[consts.id_to_district[d]][consts.id_to_district[n]][f] = w[n]

        networks.plot_egonet(influence, f'{savepath}_{name}')
