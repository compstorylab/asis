
import sys
import time
from pathlib import Path

import cli
import data_utils


def parse_args(args):
    """ Util function to parse command-line arguments """
    parser = cli.parser()

    parser.add_argument(
        'datapth',
        help='path to raw (.dat) files for Hong Kong Deaths dataset [1995-2017]'
    )

    parser.add_argument(
        '-s', '--savepath',
        default=Path.cwd() / 'hk9517.csv.gz',
        help='absolute Path to save new dataset'
    )

    return parser.parse_args(args)


def main(args=None):
    timeit = time.time()

    if args is None:
        args = sys.argv[1:]
    args = parse_args(args)

    datapth = Path(args.datapth)
    savepth = Path(args.savepath)

    data_utils.builder(datapth, savepth)

    print(f'Saved: {savepth}')
    print(f'Total time elapsed: {time.time() - timeit:.2f} sec.')


if __name__ == "__main__":
    main()
