
from functools import partial

import data_utils
import pandas as pd
import numpy as np


def get_dims(N, big_row=True):
    """Finds two integers a and b that most nearly satisfy a * b = N.
    In particular, we must have a * b >= N.

    Args:
        N: integer
        :big_row: whether to prefer more rows or columns. Default is big_row=False

    Returns:
        (a, b) tuple of integers
    """
    ceil_sqrt = np.ceil(np.sqrt(N))
    sqrt = np.sqrt(N)
    int_sqrt = int(sqrt)

    if ceil_sqrt == sqrt:
        return int_sqrt, int_sqrt
    else:
        # ceiling of sqrt is greater than sqrt
        rem = 0
        while (int_sqrt + rem) * int_sqrt < N:
            rem += 1
            continue
        if big_row:
            return (int_sqrt + rem), int_sqrt
        return int_sqrt, (int_sqrt + rem)


def get_death_records(datapath):
    """Load a dataframe of daily death records in Hong Kong

    Args:
        datapath: path to csv file

    Returns:
        (pandas.Dataframe) a dataframe of daily death records
    """
    df = pd.read_csv(datapath, header=0)
    df['date_of_death'] = pd.to_datetime(df[['year', 'month', 'day']])
    df = df.set_index('date_of_death')
    df = df.loc['1995':'2017']
    return df


def get_mma_records(datapath):
    """Load a dataframe of insurance records in Hong Kong

    Args:
        datapath: path to csv file

    Returns:
        (pandas.Dataframe) a dataframe of insurance records
    """
    hashtbl = {
        'ins_occ': 'occupation',
        'insured_tpu': 'tpu',
        'ins_age': 'age',
        'ins_sex': 'sex',
    }
    districtor = partial(data_utils.decode_tpu, districts=data_utils.district_encoder())

    df = pd.read_csv(datapath, header=0, index_col='issue_date')
    df = df.drop(['ins_ben_opt', 'ins_cvg_code', 'ins_risk_class', 'ins_ver_id', 'lapse_option'], axis=1)
    df.rename(columns=hashtbl, inplace=True)

    df['district'] = df['tpu'].apply(districtor)

    df = df[df.age < 200]

    df.index = pd.to_datetime(df.index)
    df = df.loc['1995':'2018']

    df['year'] = df.index.year
    df['month'] = df.index.month
    return df


def get_census_records(datapath, index_col):
    """Load a dataframe of annual population records in Hong Kong

    Args:
        datapath: path to csv file
        index_col: a list of indices

    Returns:
        (pandas.Dataframe) a dataframe of annual population records
    """
    if datapath.suffix == '.xlsx':
        df = pd.read_csv(datapath, header=0, na_values='-', index_col=index_col, comment='#')
        df.reset_index(inplace=True)
        df = df.rename(columns={"District Council District": "District"})
        df.to_csv(f'{datapath}.csv', index=False)
    else:
        df = pd.read_csv(
            datapath,
            header=0,
        )
    return df


def get_crosstabs(datapath):
    """Load a cross-sectional dataframe of records in Hong Kong

    Args:
        datapath: path to csv file

    Returns:
        (pandas.Dataframe) a dataframe by year and district
    """
    df = pd.read_csv(datapath, header=0)
    return df


def crosstab_gender(dea, mma, eco, savepath):
    """
    Cross reference tables at the individuals level by gender
    Args:
        dea: mortality dataset
        mma: MMA dataset
        eco: census dataset
        savepath: path to save generated dataframe
    """

    eco.columns = map(str.lower, eco.columns)
    eco['year'] = eco['year'].astype(int).astype(str)
    years = eco['year'].unique()

    df = eco.groupby(['year', 'sex', 'district']).sum()

    df['minorities'] = df['ethnic minorities'] / df["population"]
    df['single_parents'] = df['single parents'] / df["population"]

    df['unemployment'] = (df["labour force"] - df["working population"]) / df["labour force"]
    df['unemployment_minorities'] = (df["labour force"] - df["working ethnic minorities"]) / df["labour force"]

    df['homeless'] = (df["population"] - df["population in domestic households"]) / df["population"]
    df['in_school'] = df["persons attending full-time courses in educational institutions in hong kong"] \
        / df["population"]

    df['mobile_residents'] = (df['mobile residents'] / df['population'])
    df['median_income'] = df['median monthly income from main employment']

    dea['year'] = dea['year'].astype(int).astype(str)
    mortality = dea.groupby(['year', 'sex', 'district']).count().loc[years, 'age'].to_frame('deaths')
    mortality = mortality.rename({'M': 'Male', 'F': 'Female'})
    df['mortality'] = (mortality["deaths"] / df["population"]).dropna(how='all')

    mma['year'] = mma['year'].astype(int).astype(str)
    insurance = mma.groupby(['year', 'sex', 'district']).count().loc[years, 'contract_no'].to_frame('insured')
    insurance = insurance.rename({'M': 'Male', 'F': 'Female'})
    df['life_insurance'] = (insurance["insured"] / df["population"]).dropna(how='all')

    df = df.rename({'Island': 'Islands'})
    df = df.loc[:, [
        'population',
        'mortality',
        'life_insurance',
        'unemployment',
        'unemployment_minorities',
        'minorities',
        'homeless',
        'in_school',
        'mobile_residents',
        'median_income',
        'single_parents',
    ]]
    df.to_csv(savepath)


def crosstab_households(dea, mma, eco, hou, savepath):
    """
    Cross reference tables at the households level
    Args:
        dea: mortality dataset
        mma: MMA dataset
        eco: census dataset by gender
        hou: census dataset by households
        savepath: path to save generated dataframe
    """

    eco.columns = map(str.lower, eco.columns)
    eco['year'] = eco['year'].astype(int).astype(str)

    hou.columns = map(str.lower, hou.columns)
    hou['year'] = hou['year'].astype(int).astype(str)

    years = eco['year'].unique()

    df = eco.groupby(['year', 'district']).sum()

    df['minorities'] = df['ethnic minorities'] / df["population"]
    df['single_parents'] = df['single parents'] / df["population"]

    df['unemployment'] = (df["labour force"] - df["working population"]) / df["labour force"]
    df['unemployment_minorities'] = (df["labour force"] - df["working ethnic minorities"]) / df["labour force"]

    df['homeless'] = (df["population"] - df["population in domestic households"]) / df["population"]
    df['in_school'] = df["persons attending full-time courses in educational institutions in hong kong"] \
        / df["population"]

    df['mobile_residents'] = (df['mobile residents'] / df['population'])
    df['median_income'] = df['median monthly income from main employment']

    dea['year'] = dea['year'].astype(int).astype(str)
    mortality = dea.groupby(['year', 'district']).count().loc[years, 'age'].to_frame('deaths')
    df['mortality'] = (mortality["deaths"] / df["population"]).dropna(how='all')

    mma['year'] = mma['year'].astype(int).astype(str)
    insurance = mma.groupby(['year', 'district']).count().loc[years, 'contract_no'].to_frame('insured')
    df['life_insurance'] = (insurance["insured"] / df["population"]).dropna(how='all')

    hou = hou.set_index(['year', 'district'])
    df['median_rent_to_income_ratio'] = hou['median rent to income ratio']
    df['median_monthly_household_income'] = hou['median monthly domestic household income']
    df['elderly_households'] = hou["elderly households (aged 65 and over)"] / hou["domestic households"]
    df['young_households'] = hou["domestic households with children (aged under 15)"] / hou["domestic households"]
    df['unemployment_households'] = (hou["domestic households"] - hou["economically active domestic households"]) \
        / hou["domestic households"]

    df = df.rename({'Island': 'Islands'})
    df = df.loc[:, [
        'population',
        'mortality',
        'life_insurance',
        'unemployment',
        'unemployment_minorities',
        'unemployment_households',
        'minorities',
        'homeless',
        'in_school',
        'mobile_residents',
        'median_income',
        'single_parents',
        'median_rent_to_income_ratio',
        'median_monthly_household_income',
        'elderly_households',
        'young_households',
    ]]
    df.to_csv(savepath)
