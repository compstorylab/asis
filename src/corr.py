
from matplotlib import colors
import esda
import libpysal as lps
import mapclassify as mc
import seaborn as sns
import matplotlib.pyplot as plt


def plot_corr(df, savepath):
    """
    Plot correlation matrix

    Args:
        df (pandas.Dataframe): a dataframe where rows are records and columns are features
        savepath (str): absolute path to save plot
    """
    fig = plt.figure(figsize=(6, 6))
    mat = df.corr()

    print('Correlation Coefficients:')
    print(mat.round(2).loc[:, 'mortality'])

    ax = sns.heatmap(
        data=mat,
        cmap='RdGy_r',
        center=0,
        vmax=1,
        vmin=-1,
        square=True,
        linewidths=.5,
        cbar_kws=dict(
            extend="both",
            shrink=.6,
            pad=.01
        )
    )

    plt.tight_layout()
    plt.savefig(f'{savepath}.png', dpi=200, bbox_inches='tight', pad_inches=.25)


def plot_autocorrelation(df, gdf, savepath):

    fig, axes = plt.subplots(ncols=3, nrows=3, figsize=(12, 12), subplot_kw={'aspect': 'equal'})

    for i, year in enumerate([2006, 2001, 2016]):

        d = df[df.year == year][['district', 'mortality']].set_index('district')
        d = (d - d.mean()) / d.std()

        if 'mortality' not in gdf.columns:
            gdf = gdf.combine_first(d)
        else:
            gdf.update(d)

        y = gdf['mortality']
        cls = mc.Quantiles(gdf['mortality'], k=5)
        wq = lps.weights.Queen.from_dataframe(gdf)
        wq.transform = 'r'
        li = esda.moran.Moran_Local(y, wq)
        sig = 1 * (li.p_sim < 0.1)

        hotspot = 1 * (sig * li.q == 1)
        coldspot = 3 * (sig * li.q == 3)
        doughnut = 2 * (sig * li.q == 2)
        diamond = 4 * (sig * li.q == 4)
        spots = hotspot + coldspot + doughnut + diamond

        spot_labels = ['0 Neutral', '1 Hotspot', '2 Doughnut', '3 Coldspot', '4 Diamond']
        hmap = colors.ListedColormap(['w', 'red', 'lightblue', 'blue', 'pink'])
        labels = [spot_labels[i] for i in spots]

        gdf.plot(
            ax=axes[i, 0],
            column='mortality',
            edgecolor='k',
            cmap='RdGy_r',
            vmin=-1.5,
            vmax=1.5,
        )
        axes[i, 0].set_axis_off()

        gdf.assign(cl=cls.yb).plot(
            ax=axes[i, 1],
            column='cl',
            categorical=True,
            edgecolor='k',
            cmap='OrRd',
        )
        axes[i, 1].set_axis_off()

        gdf.assign(cl=labels).plot(
            ax=axes[i, 2],
            column='cl',
            categorical=True,
            edgecolor='k',
            cmap=hmap,
        )
        axes[i, 2].set_axis_off()

    axes[0, 0].set_title('Normalized mortality rate')
    axes[0, 1].set_title('Spatial lag (Quantiles)')
    axes[0, 2].set_title('Local autocorrelation')
    plt.tight_layout()
    plt.subplots_adjust(top=0.97, right=0.97, wspace=-0.1, hspace=-0.5)
    plt.savefig(f'{savepath}.png', dpi=200, bbox_inches='tight', pad_inches=.25)
