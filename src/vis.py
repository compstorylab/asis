
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib.lines import Line2D
from pandas.plotting import register_matplotlib_converters

register_matplotlib_converters()

import consts


def plot_monthly_deaths(df, savepath):
    """Plot monthly death records
    Args:
        df (pandas.Dataframe): a dataframe where rows are records and columns are features
        savepath (str): absolute path to save plot
    """

    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
    })
    date_format = '%Y'
    major_locator = mdates.YearLocator(2)
    minor_locator = mdates.MonthLocator([1, 4, 7, 10])
    fig, (ax, ax2) = plt.subplots(figsize=(10, 8), nrows=2)

    ordered_list = [
        "Sep", "Oct", "Nov", "Dec",
        "Jan", "Feb", "Mar", "Apr",
        "May", "Jun", "Jul", "Aug",
    ]

    ts = df.resample('M').count().month

    monthly_deaths = df.groupby(['year', 'month']).age.count().to_frame('total').reset_index()
    monthly_deaths.year = pd.to_datetime(monthly_deaths.year.astype(int), format='%Y').dt.strftime('%Y')
    monthly_deaths.month = pd.to_datetime(monthly_deaths.month.astype(int), format='%m').dt.strftime('%b')

    ax.annotate(
        'A', xy=(-.1, 1.), color='k', weight='bold',
        xycoords="axes fraction", fontsize=16,
    )

    ax.plot(
        ts.idxmax(), ts.max(),
        'o', ms=15, color='red', alpha=0.5
    )

    ax.annotate(
        f"{ts.idxmax().strftime('%b %Y')}\n{round(ts.max(), 2)} deaths",
        xy=(ts.idxmax(), ts.max()+400),
        ha='center',
        verticalalignment='center',
        color='red',
        xycoords="data",
    )

    ax.plot(
        ts,
        marker='o',
        ms=3,
        color='grey',
        mfc='grey',
        mec='grey',
        lw=0,
    )

    ax.plot(
        ts.rolling(12, center=True).mean(),
        color='k',
        lw=1,
    )

    ax.set_xlabel('')
    ax.set_ylabel('Number of deaths')
    ax.set_ylim(1000, 5000)
    ax.grid(True, which="both", axis='both', alpha=.3, lw=1, linestyle='-')
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.xaxis.set_major_locator(major_locator)
    ax.xaxis.set_major_formatter(mdates.DateFormatter(date_format))
    ax.xaxis.set_minor_locator(minor_locator)

    ax2.annotate(
        'B', xy=(-.1, 1.), color='k', weight='bold',
        xycoords="axes fraction", fontsize=16,
    )

    for y in monthly_deaths.year.unique():
        ts = monthly_deaths[monthly_deaths.year == y].set_index('month')
        ts = ts.reindex(ordered_list)

        ax2.plot(
            ts.total,
            marker='o',
            ms=3,
            color=consts.cmap[y],
            mfc=consts.cmap[y],
            mec=consts.cmap[y],
            lw=0,
        )

    avg = monthly_deaths[monthly_deaths['year'].isin(map(str, consts.range_1))].groupby('month').mean()
    avg = avg.reindex(ordered_list)
    ax2.plot(avg, color='C0', alpha=.5)

    avg = monthly_deaths[monthly_deaths['year'].isin(map(str, consts.range_2))].groupby('month').mean()
    avg = avg.reindex(ordered_list)
    ax2.plot(avg, color='C1', alpha=.5)

    avg = monthly_deaths[monthly_deaths['year'].isin(map(str, consts.range_3))].groupby('month').mean()
    avg = avg.reindex(ordered_list)
    ax2.plot(avg, color='C2', alpha=.5)

    avg_deaths = monthly_deaths.groupby('month').mean()
    avg_deaths = avg_deaths.reindex(ordered_list)
    ax2.plot(avg_deaths, color='k', label=r'$E[y]$')

    ax2.set_xlabel('')
    ax2.set_ylabel('Average number of deaths')
    ax2.grid(True, which="both", axis='both', alpha=.3, lw=1, linestyle='-')
    ax2.spines['right'].set_visible(False)
    ax2.spines['top'].set_visible(False)

    legend_colors = [
        Line2D([0], [0], color='C0', lw=0, marker='o', ms=5),
        Line2D([0], [0], color='C1', lw=0, marker='o', ms=5),
        Line2D([0], [0], color='C2', lw=0, marker='o', ms=5),
        Line2D([0], [0], color='k', lw=2)
    ]
    legend_labels = ["2010's", "2000's", "1990's", r'$E[y]$']

    ax2.legend(
        legend_colors,
        legend_labels,
        frameon=False
    )

    plt.tight_layout()
    plt.savefig(f'{savepath}.png', dpi=200, bbox_inches='tight', pad_inches=.25)
    plt.savefig(f'{savepath}.pdf', bbox_inches='tight', pad_inches=.25)


def plot_age_dist(df, savepath):
    """Plot age distribution
    Args:
        df (pandas.Dataframe): a dataframe where rows are records and columns are features
        savepath (str): absolute path to save plot
    """

    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
    })

    date_format = '%Y'
    major_locator = mdates.YearLocator(2)
    minor_locator = mdates.MonthLocator([1, 4, 7, 10])

    fig = plt.figure(figsize=(10, 8))
    gs = fig.add_gridspec(ncols=2, nrows=2)
    axm = fig.add_subplot(gs[0, 0])
    axf = fig.add_subplot(gs[0, 1], sharey=axm)
    axt = fig.add_subplot(gs[1, :])

    ddf = df.groupby(['year', 'sex', 'age']).age.count().to_frame('total').reset_index()
    ddf.year = pd.to_datetime(ddf.year.astype(int), format='%Y').dt.strftime('%Y')

    for y in ddf.year.unique():
        for ax, s, ll in zip([axm, axf], ['M', 'F'], ['Male', 'Female']):
            ts = ddf[(ddf.year == y) & (ddf.sex == s)].set_index('age')
            ax.plot(
                ts.total,
                marker='o',
                ms=3,
                color=consts.cmap[y],
                mfc=consts.cmap[y],
                mec=consts.cmap[y],
                lw=0,
                alpha=.15
            )

            ax.set_title(ll)
            ax.set_ylim(0, 900)
            ax.set_xlim(0, 120)
            ax.set_xticks(range(0, 140, 20))
            ax.set_xticks(range(0, 130, 10), minor=True)
            ax.set_xlabel('Age (years)')
            ax.grid(True, which="both", axis='both', alpha=.3, lw=1, linestyle='-')
            ax.spines['right'].set_visible(False)
            ax.spines['top'].set_visible(False)

    for g, c in zip(consts.year_groups, consts.group_colors):
            avg = ddf[
                ddf['year'].isin(map(str, g))
            ].groupby(['age', 'sex']).mean().reset_index()

            for ax, s in zip([axm, axf], ['M', 'F']):
                ts = avg[avg.sex == s].set_index('age')
                ax.plot(ts.total, color=c)

    legend_colors = [
        Line2D([0], [0], color='C0', lw=0, marker='o', ms=5),
        Line2D([0], [0], color='C1', lw=0, marker='o', ms=5),
        Line2D([0], [0], color='C2', lw=0, marker='o', ms=5),
        Line2D([0], [0], color='k', lw=2)
    ]
    legend_labels = ["2010's", "2000's", "1990's", r'$E[y]$']

    axm.legend(
        legend_colors,
        legend_labels,
        frameon=False
    )
    axm.set_ylabel('Annual number of deaths')

    ts = df.groupby('sex', 'index')['age'].resample('M').count()
    for s, c, l in zip(['M', 'F'], ['grey', 'red'], ('Male', 'Female')):
        axt.plot(
            ts[s],
            marker='o',
            ms=3,
            color=c,
            mfc=c,
            mec=c,
            lw=0,
            alpha=.5,
            label=''
        )

        axt.plot(
            ts[s].rolling(12, center=True).mean(),
            color=c,
            lw=3,
            label=l
        )

    axt.set_xlabel('')
    axt.set_ylabel('Number of deaths per month')
    axt.grid(True, which="both", axis='both', alpha=.3, lw=1, linestyle='-')
    axt.legend(frameon=False)

    axt.spines['right'].set_visible(False)
    axt.spines['top'].set_visible(False)
    axt.xaxis.set_major_locator(major_locator)
    axt.xaxis.set_major_formatter(mdates.DateFormatter(date_format))
    axt.xaxis.set_minor_locator(minor_locator)

    axm.annotate(
        'A', xy=(-.2, 1.), color='k', weight='bold',
        xycoords="axes fraction", fontsize=16,
    )

    axf.annotate(
        'B', xy=(-.2, 1.), color='k', weight='bold',
        xycoords="axes fraction", fontsize=16,
    )

    axt.annotate(
        'C', xy=(-.09, 1.), color='k', weight='bold',
        xycoords="axes fraction", fontsize=16,
    )

    plt.tight_layout()
    plt.savefig(f'{savepath}.png', dpi=200, bbox_inches='tight', pad_inches=.25)


def plot_residency(df, savepath):
    """Plot length of staying in Hong Kong
    Args:
        df (pandas.Dataframe): a dataframe where rows are records and columns are features
        savepath (str): absolute path to save plot
    """

    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
    })

    residency = df.groupby(['length_of_stay_in_hongkong'], as_index=False).size().to_frame('total')
    residency = residency.sort_values(by='total', ascending=False)
    residency['rank'] = residency['total'].rank(method='average', ascending=False)
    total = residency['total'].sum()
    residency['freq'] = residency['total'] / total
    residency = residency.sort_values(by='total', ascending=False)

    fig, ax = plt.subplots(figsize=(8, 8))
    ax.loglog(residency['total'], '.', color='grey')

    ax.set_xlabel('Length of staying in Hong Kong (years)')
    ax.set_ylabel('Number of deaths')
    ax.grid(True, which="both", axis='both', alpha=.3, lw=1, linestyle='-')
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)

    plt.tight_layout()
    plt.savefig(f'{savepath}.png', dpi=200, bbox_inches='tight', pad_inches=.25)


def plot_nationality(df, savepath):
    """Plot average age by country of previous residence
    Args:
        df (pandas.Dataframe): a dataframe where rows are records and columns are features
        savepath (str): absolute path to save plot
    """

    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
    })

    origin = df.groupby(['country_of_previous_residence'])['age']
    origin = origin.resample('Y').mean().unstack(level=0)

    fig, ax = plt.subplots(figsize=(10, 10))
    #ax = sns.boxenplot(data=origin, ax=ax, linewidth=1, orient='h', cut=0, palette='tab20')
    ax = sns.violinplot(data=origin, ax=ax, linewidth=1, orient='h', cut=0, palette='tab20')

    ax.grid(True, which="both", axis='x', alpha=.3, lw=1, linestyle='-')
    ax.set_ylabel('Country of previous residence')
    ax.set_xlabel('Annual average age')
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.set_xlim(20, 100)

    plt.tight_layout()
    plt.savefig(f'{savepath}.png', dpi=200, bbox_inches='tight', pad_inches=.25)


def plot_occupation(df, savepath):
    """Plot average age by occupation
    Args:
        df (pandas.Dataframe): a dataframe where rows are records and columns are features
        savepath (str): absolute path to save plot
    """

    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
    })

    origin = df.groupby(['occupation'])['age']
    origin = origin.resample('M').mean().unstack(level=0)

    fig, ax = plt.subplots(figsize=(10, 10))
    ax = sns.violinplot(data=origin, ax=ax, linewidth=1, orient='h', cut=0, palette='magma')

    ax.grid(True, which="both", axis='x', alpha=.3, lw=1, linestyle='-')
    ax.set_ylabel('Occupation')
    ax.set_xlabel('Monthly average age')
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.set_xlim(20, 100)

    plt.tight_layout()
    plt.savefig(f'{savepath}.png', dpi=200, bbox_inches='tight', pad_inches=.25)


def plot_mma_age_dist(df, savepath):
    """Plot age distribution
    Args:
        df (pandas.Dataframe): a dataframe where rows are records and columns are features
        savepath (str): absolute path to save plot
    """

    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
    })

    date_format = '%Y'
    major_locator = mdates.YearLocator(2)
    minor_locator = mdates.MonthLocator([1, 4, 7, 10])

    fig = plt.figure(figsize=(10, 8))
    gs = fig.add_gridspec(ncols=2, nrows=2)
    axt = fig.add_subplot(gs[0, :])
    axm = fig.add_subplot(gs[1, 0])
    axf = fig.add_subplot(gs[1, 1], sharey=axm)

    ts = pd.crosstab(
        index=df.index,
        columns=df.sex,
        values=df.contract_no,
        aggfunc='count'
    ).resample('M').sum()

    for s, c, l in zip(['M', 'F'], ['grey', 'red'], ('Male', 'Female')):
        axt.plot(
            ts[s],
            marker='o',
            ms=3,
            color=c,
            mfc=c,
            mec=c,
            lw=0,
            alpha=.5,
            label=''
        )

        axt.plot(
            ts[s].rolling(12, center=True).mean(),
            color=c,
            lw=3,
            label=l
        )

    axt.set_xlabel('')
    axt.set_ylabel('Number of records per month')
    axt.grid(True, which="both", axis='both', alpha=.3, lw=1, linestyle='-')
    axt.legend(frameon=False)

    axt.spines['right'].set_visible(False)
    axt.spines['top'].set_visible(False)
    axt.xaxis.set_major_locator(major_locator)
    axt.xaxis.set_major_formatter(mdates.DateFormatter(date_format))
    axt.xaxis.set_minor_locator(minor_locator)

    ddf = df.groupby(['year', 'sex', 'age']).age.count().to_frame('total').reset_index()
    ddf.year = pd.to_datetime(ddf.year.astype(int), format='%Y').dt.strftime('%Y')

    for y in ddf.year.unique():
        for ax, s, ll in zip([axm, axf], ['M', 'F'], ['Male', 'Female']):
            ts = ddf[(ddf.year == y) & (ddf.sex == s)].set_index('age')
            ax.plot(
                ts.total,
                marker='o',
                ms=3,
                color=consts.cmap[y],
                mfc=consts.cmap[y],
                mec=consts.cmap[y],
                lw=0,
                alpha=.15
            )

            ax.set_title(ll)
            ax.set_ylim(0, 500)
            ax.set_xlim(0, 80)
            ax.set_xticks(range(0, 90, 10))
            ax.set_xticks(range(0, 80, 5), minor=True)
            ax.set_xlabel('Age (years)')
            ax.grid(True, which="both", axis='both', alpha=.3, lw=1, linestyle='-')
            ax.spines['right'].set_visible(False)
            ax.spines['top'].set_visible(False)

    for g, c in zip(consts.year_groups, consts.group_colors):
            avg = ddf[
                ddf['year'].isin(map(str, g))
            ].groupby(['age', 'sex']).mean().reset_index()

            for ax, s in zip([axm, axf], ['M', 'F']):
                ts = avg[avg.sex == s].set_index('age')
                ax.plot(ts.total, color=c)

    legend_colors = [
        Line2D([0], [0], color='C0', lw=0, marker='o', ms=5),
        Line2D([0], [0], color='C1', lw=0, marker='o', ms=5),
        Line2D([0], [0], color='C2', lw=0, marker='o', ms=5),
        Line2D([0], [0], color='k', lw=2)
    ]
    legend_labels = ["2010's", "2000's", "1990's", r'$E[y]$']

    axm.legend(
        legend_colors,
        legend_labels,
        frameon=False
    )
    axm.set_ylabel('Annual number of records')

    axt.annotate(
        'A', xy=(-.09, 1.), color='k', weight='bold',
        xycoords="axes fraction", fontsize=16,
    )

    axm.annotate(
        'B', xy=(-.2, 1.), color='k', weight='bold',
        xycoords="axes fraction", fontsize=16,
    )

    axf.annotate(
        'C', xy=(-.2, 1.), color='k', weight='bold',
        xycoords="axes fraction", fontsize=16,
    )

    plt.tight_layout()
    plt.savefig(f'{savepath}.png', dpi=200, bbox_inches='tight', pad_inches=.25)


def plot_population(df, savepath):
    """Plot population stats by district
    Args:
        df (pandas.Dataframe): a dataframe where rows are records and columns are features
        savepath (str): absolute path to save plot
    """

    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
    })

    df = df.replace({'Central and Western': 'Central\n& Western'})
    df['Unemployment'] = (df["Labour Force"] - df["Working Population"]) / df["Labour Force"]
    unemployment_median = df['Unemployment'].median()
    df['Unemployment'] -= unemployment_median
    df['Unemployment'] *= 100

    df['Homeless'] = (df["Population"] - df["Population in Domestic Households"]) / df["Population"]
    homeless_median = df['Homeless'].median()
    df['Homeless'] -= homeless_median
    df['Homeless'] *= 100

    df['School'] = df["Persons Attending Full-time Courses in Educational Institutions in Hong Kong"] \
        / df["Population"]
    school_median = df['School'].median()
    df['School'] -= school_median
    df['School'] *= 100

    df['Mobile'] = (df['Mobile Residents'] / df['Population'])
    mobile_median = df['Mobile'].median()
    df['Mobile'] -= mobile_median
    df['Mobile'] *= 100

    df['Income'] = df['Median Monthly Income from Main Employment']
    income_median = df['Income'].median()
    df['Income'] -= income_median

    for var, palette in zip(['Year', 'Sex'], ['rocket_r', {'Male': 'grey', 'Female': 'red'}]):
        fig, axes = plt.subplots(figsize=(14, 14), nrows=5, sharex=True)

        sns.barplot(
            ax=axes[0],
            data=df,
            x="District",
            y="Unemployment",
            hue=var,
            palette=palette,
            alpha=.85
        )
        axes[0].axhline(0, color="k", clip_on=False)
        axes[0].set_xlabel('')
        axes[0].set_ylabel('Unemployment\nRate (%)')
        axes[0].legend(frameon=False)
        axes[0].set_yticks(range(-2, 4, 1))
        axes[0].annotate(
            f"Median Rate = {round(unemployment_median * 100, 2)}%",
            xy=(.4, 1.),
            color='k',
            xycoords="axes fraction",
            fontsize=14,
        )

        sns.barplot(
            ax=axes[1],
            data=df,
            x="District",
            y="Homeless",
            hue=var,
            palette=palette,
            alpha=.85
        )
        axes[1].axhline(0, color="k", clip_on=False)
        axes[1].set_xlabel('')
        axes[1].set_ylabel('Homeless\nRate (%)')
        axes[1].get_legend().remove()
        axes[1].set_yticks(range(-1, 4, 1))
        axes[1].annotate(
            f"Median Rate = {round(homeless_median * 100, 2)}%",
            xy=(.4, 1.),
            color='k',
            xycoords="axes fraction",
            fontsize=14,
        )

        sns.barplot(
            ax=axes[2],
            data=df,
            x="District",
            y="School",
            hue=var,
            palette=palette,
            alpha=.85
        )
        axes[2].set_xlabel('')
        axes[2].axhline(0, color="k", clip_on=False)
        axes[2].set_ylabel('Attending School\nRate (%)')
        axes[2].set_yticks(range(-6, 8, 2))
        axes[2].get_legend().remove()
        axes[2].ticklabel_format(axis='y', style='sci', useMathText=True, scilimits=(0, 3))
        axes[2].annotate(
            f"Median Rate = {round(school_median * 100, 2)}%",
            xy=(.4, 1.),
            color='k',
            xycoords="axes fraction",
            fontsize=14,
        )

        sns.barplot(
            ax=axes[3],
            data=df,
            x="District",
            y="Mobile",
            hue=var,
            palette=palette,
            alpha=.85
        )
        axes[3].set_xlabel('')
        axes[3].axhline(0, color="k", clip_on=False)
        axes[3].set_ylabel('Mobile Residents\nRate (%)')
        axes[3].get_legend().remove()
        axes[3].ticklabel_format(axis='y', style='sci', useMathText=True, scilimits=(0, 3))
        axes[3].annotate(
            f"Median Rate = {round(mobile_median * 100, 2)}%",
            xy=(.4, 1.),
            color='k',
            xycoords="axes fraction",
            fontsize=14,
        )

        sns.barplot(
            ax=axes[-1],
            data=df,
            x="District",
            y="Income",
            hue=var,
            palette=palette,
            alpha=.85
        )
        axes[-1].set_xlabel('')
        axes[-1].axhline(0, color="k", clip_on=False)
        axes[-1].set_ylabel('Median Monthly\nIncome (HKD)')
        axes[-1].get_legend().remove()
        axes[-1].set_yticks(range(-5000, 15000, 5000))
        axes[-1].ticklabel_format(axis='y', style='sci', useMathText=True, scilimits=(-3, 3))
        axes[-1].annotate(
            f"Median = {round(income_median/10**4, 2)} x $10^4$ (HKD)",
            xy=(.4, 1.),
            color='k',
            xycoords="axes fraction",
            fontsize=14,
        )

        for ax, label in zip(axes, 'A B C D E'.split(' ')):
            ax.annotate(
                label, xy=(-.06, 1.), color='k', weight='bold',
                xycoords="axes fraction", fontsize=16,
            )

        sns.despine(offset=5, trim=True)
        axes[-1].set_xticklabels(df['District'], rotation=45, ha='center')

        plt.tight_layout()
        plt.savefig(f'{savepath}_{var}.png', dpi=200, bbox_inches='tight', pad_inches=.25)


def plot_household(df, savepath):
    """Plot household stats by district
    Args:
        df (pandas.Dataframe): a dataframe where rows are records and columns are features
        savepath (str): absolute path to save plot
    """

    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
    })
    palette = 'rocket_r'

    df = df.replace({'Central and Western': 'Central\n& Western'})
    df['Rent'] = df["Median Rent to Income Ratio"]
    rent_median = df['Rent'].median()
    df['Rent'] -= rent_median

    df['Elderly'] = df["Elderly Households (Aged 65 and over)"] / df["Domestic Households"]
    elderly_median = df['Elderly'].median()
    df['Elderly'] -= elderly_median
    df['Elderly'] *= 100

    df['Unemployment'] = (df["Domestic Households"] - df["Economically Active Domestic Households"]) / df["Domestic Households"]
    unemployment_median = df['Unemployment'].median()
    df['Unemployment'] -= unemployment_median
    df['Unemployment'] *= 100

    df['Children'] = df["Domestic Households with Children (Aged under 15)"] / df["Domestic Households"]
    children_median = df['Children'].median()
    df['Children'] -= children_median
    df['Children'] *= 100

    fig, axes = plt.subplots(figsize=(14, 14), nrows=4, sharex=True)

    sns.barplot(
        ax=axes[0],
        data=df,
        x="District",
        y="Rent",
        hue='Year',
        palette=palette,
        alpha=.85
    )
    axes[0].axhline(0, color="k", clip_on=False)
    axes[0].set_xlabel('')
    axes[0].set_ylabel('Median Rent to\nIncome Rate (%)')
    axes[0].legend(frameon=False)
    axes[0].set_yticks(range(-5, 20, 5))
    axes[0].annotate(
        f"Median Rate = {round(rent_median, 2)}%",
        xy=(.4, 1.),
        color='k',
        xycoords="axes fraction",
        fontsize=14,
    )

    sns.barplot(
        ax=axes[1],
        data=df,
        x="District",
        y="Elderly",
        hue='Year',
        palette=palette,
        alpha=.85
    )
    axes[1].axhline(0, color="k", clip_on=False)
    axes[1].set_xlabel('')
    axes[1].set_ylabel('Elderly Rate (%)\n[Aged 65 and over]')
    axes[1].get_legend().remove()
    axes[1].set_yticks(range(-4, 4, 1))
    axes[1].annotate(
        f"Median Rate = {round(elderly_median * 100, 2)}%",
        xy=(.4, 1.),
        color='k',
        xycoords="axes fraction",
        fontsize=14,
    )

    sns.barplot(
        ax=axes[2],
        data=df,
        x="District",
        y="Unemployment",
        hue='Year',
        palette=palette,
        alpha=.85
    )
    axes[2].axhline(0, color="k", clip_on=False)
    axes[2].set_xlabel('')
    axes[2].set_ylabel('Unemployment Rate (%)')
    axes[2].get_legend().remove()
    axes[2].annotate(
        f"Median Rate = {round(unemployment_median * 100, 2)}%",
        xy=(.4, 1.),
        color='k',
        xycoords="axes fraction",
        fontsize=14,
    )

    sns.barplot(
        ax=axes[-1],
        data=df,
        x="District",
        y="Children",
        hue='Year',
        palette=palette,
        alpha=.85
    )
    axes[-1].axhline(0, color="k", clip_on=False)
    axes[-1].set_xlabel('')
    axes[-1].set_ylabel('Children Rate (%)\n[Aged under 15]')
    axes[-1].get_legend().remove()
    axes[-1].set_yticks(range(-6, 12, 2))
    axes[-1].annotate(
        f"Median Rate = {round(children_median * 100, 2)}%",
        xy=(.4, 1.),
        color='k',
        xycoords="axes fraction",
        fontsize=14,
    )

    for ax, label in zip(axes, 'A B C D'.split(' ')):
        ax.annotate(
            label, xy=(-.06, 1.), color='k', weight='bold',
            xycoords="axes fraction", fontsize=16,
        )

    sns.despine(offset=5, trim=True)
    axes[-1].set_xticklabels(df['District'], rotation=45, ha='center')

    plt.tight_layout()
    plt.savefig(f'{savepath}.png', dpi=200, bbox_inches='tight', pad_inches=.25)
    plt.savefig(f'{savepath}.pdf', bbox_inches='tight', pad_inches=.25)


def plot_district_attrs(dea, mma, eco, hou, savepath):
    """Plot population attrs by district
    Args:
        df (pandas.Dataframe): a dataframe where rows are records and columns are features
        savepath (str): absolute path to save plot
    """
    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
    })

    eco['Year'] = eco['Year'].astype(str)
    years = eco['Year'].unique()
    pop = pd.crosstab(
        index=eco.District,
        columns=eco.Year,
        values=eco.Population,
        aggfunc='sum'
    )

    eco["Unemployment"] = (
        (eco["Labour Force"] - eco["Working Population"]) * 10**3
    ) / eco["Labour Force"]
    unemployment = pd.crosstab(
        index=eco.District,
        columns=eco.Year,
        values=eco.Unemployment,
        aggfunc='sum'
    )

    eco['Homeless'] = (
        (eco["Population"] - eco["Population in Domestic Households"]) * 10**3
    ) / eco["Population"]
    homeless = pd.crosstab(
        index=eco.District,
        columns=eco.Year,
        values=eco.Homeless,
        aggfunc='sum'
    )

    eco['Mobile'] = (eco['Mobile Residents'] / eco['Population']) * 10**3
    mobile = pd.crosstab(
        index=eco.District,
        columns=eco.Year,
        values=eco.Mobile,
        aggfunc='sum'
    )

    dea['year'] = dea['year'].astype(int).astype(str)
    deaths = pd.crosstab(
        index=dea.district,
        columns=dea.year,
        values=dea.age,
        aggfunc='count'
    )[years]

    mma['year'] = mma['year'].astype(str)
    policies = pd.crosstab(
        index=mma.district,
        columns=mma.year,
        values=mma.age,
        aggfunc='count'
    )[years]

    mortality_rates = ((deaths * 10**3) / pop).dropna(how='all')
    insured_rates = ((policies * 10**3) / pop).dropna(how='all')

    for y in years:
        districts = pd.DataFrame(
            data={
                'district': mortality_rates.index,
                'Mortality': mortality_rates[y],
                'Insurance': insured_rates[y],
                'Unemployment': unemployment[y],
                'Homeless': homeless[y],
                'Mobile': mobile[y],
            }
        )

        x = districts.sort_values('Mortality', ascending=False)
        g = sns.PairGrid(
            x,
            x_vars=x.columns[1:],
            y_vars='district',
            height=10,
            aspect=.5
        )
        g.fig.set_size_inches(14, 7)
        g.map(
            sns.stripplot, size=10, orient="h",
            color="k", linewidth=1, edgecolor="w"
        )

        g.set(xlabel='', ylabel="")

        labels = 'A B C D E'.split(' ')
        for i, (ax, title, label) in enumerate(zip(g.axes.flat, x.columns[1:], labels)):
            ax.set(title=title)
            ax.grid(True, which="both", axis='both', lw=1, linestyle='-')
            ax.annotate(
                label, xy=(-.1, 1.), color='k', weight='bold',
                xycoords="axes fraction", fontsize=16,
            )
            avg = x.iloc[:, i+1].mean()
            ax.axvline(
                avg,
                ls='--',
                lw=2,
                color='r',
                alpha=.75
            )
            ax.set_xlabel(f'$E[x] = {round(avg, 2)}$\n(per 1,000 individuals)')

        sns.despine(left=True, bottom=True)

        plt.tight_layout()
        plt.savefig(f'{savepath}_{y}.png', dpi=200, bbox_inches='tight', pad_inches=.25)
        plt.savefig(f'{savepath}_{y}.pdf', bbox_inches='tight', pad_inches=.25)
