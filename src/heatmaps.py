
import matplotlib.colorbar as colorbar
import matplotlib.colors as mcolors
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib.lines import Line2D
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from pandas.plotting import register_matplotlib_converters

register_matplotlib_converters()

import consts


def plot_missing_data(df, savepath):
    """Plot fraction of missing data
    Args:
        df (pandas.Dataframe): a dataframe where rows are records and columns are features
        savepath (str): absolute path to save plot
    """

    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
    })

    fig, ax = plt.subplots(figsize=(10, 5))

    features = [
        f.upper() if len(f.split('_')) == 1 else ''.join([i[0] for i in f.split('_')]).upper()
        for f in df.columns
    ]

    ax = sns.heatmap(
        df.isnull(),
        cbar=False,
        vmin=0, vmax=1,
        yticklabels=[],
        xticklabels=features
    )
    ax.tick_params(axis='x', rotation=45)
    ax.set_ylabel('Records')

    plt.subplots_adjust(top=0.97, right=0.97, hspace=0.25)
    plt.savefig(f'{savepath}.png', dpi=200, bbox_inches='tight', pad_inches=.25)


def plot_tpu_table(df, savepath, metric='deaths'):
    """Plot a table of TPUs
    Args:
        df (pandas.Dataframe): a dataframe where rows are records and columns are features
        savepath (str): absolute path to save plot
    """

    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
    })

    cols = 6

    if metric == 'age':
        vmin = 20
        vmax = 100
        bounds = np.arange(vmin, vmax+5, step=5)
        xlabels = [f'{y}\nYears' for y in bounds]
        table = df.groupby(['tpu'], as_index=True).age.mean().to_frame('age')
        table = table[(table.T != 0).any()]
        table.sort_values(by=['age'], ascending=False, inplace=True)

    elif metric == 'deaths':
        vmin = 2500
        vmax = 35000
        bounds = np.arange(vmin, vmax+2500, step=2500)
        xlabels = [f'{y}\nDeaths' for y in bounds]
        table = df.groupby(['tpu'], as_index=True).age.size().to_frame('deaths')
        table = table[(table.T != 0).any()]
        table.sort_values(by=['deaths'], ascending=False, inplace=True)

    cmap = plt.cm.get_cmap('magma_r')
    cmaplist = [cmap(i) for i in range(cmap.N)]
    cmaplist[0] = (1, 1, 1, 1.0)  # force the first color entry to be white
    cmap = mcolors.LinearSegmentedColormap.from_list(None, cmaplist, cmap.N)
    norm = mcolors.BoundaryNorm(bounds, cmap.N)

    fig, axes = plt.subplots(figsize=(12, 14), ncols=cols)
    dfs = np.array_split(table, cols)

    cbarax = inset_axes(
        axes[3],
        width="850%",
        height="2%",
        bbox_to_anchor=(3, .05, 1, 1),
        bbox_transform=axes[3].transAxes,
        borderpad=.25,
    )

    for df, ax in zip(dfs, axes):
        ax = sns.heatmap(
            df,
            ax=ax,
            cbar_ax=cbarax,
            cbar=True,
            cmap=cmap,
            norm=norm,
            vmin=vmin,
            vmax=vmax,
            linewidths=0.1,
            linecolor='k',
        )

        ax.tick_params(
            axis='x',
            which='both',
            bottom=False,
            labelbottom=False
        )
        ax.set_ylabel('')
        ax.grid(False, which="both")

    cb = colorbar.ColorbarBase(
        cbarax,
        cmap=cmap,
        norm=norm,
        spacing='proportional',
        ticks=bounds,
        boundaries=bounds,
        orientation='horizontal',
        extend='both'
    )

    cbarax.tick_params(labelsize=14)
    cbarax.set_xticklabels(xlabels)
    cbarax.xaxis.set_ticks_position('top')
    cbarax.yaxis.set_label_position('left')

    plt.tight_layout()
    plt.savefig(f'{savepath}.png', dpi=200, bbox_inches='tight', pad_inches=.25)
    plt.savefig(f'{savepath}.pdf', bbox_inches='tight', pad_inches=.25)


def plot_tpu_features(df, savepath):
    """Plot features for TPUs
    Args:
        df (pandas.Dataframe): a dataframe where rows are records and columns are features
        savepath (str): absolute path to save plot
    """

    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
    })

    fig = plt.figure(figsize=(10, 8))
    gs = fig.add_gridspec(ncols=3, nrows=2)
    ax = fig.add_subplot(gs[:, :2])
    axb = fig.add_subplot(gs[0, -1])
    axc = fig.add_subplot(gs[1, -1])

    ax.annotate(
        'A', xy=(-.05, 1.), color='k', weight='bold',
        xycoords="axes fraction", fontsize=16,
    )

    axb.annotate(
        'B', xy=(-.3, 1.), color='k', weight='bold',
        xycoords="axes fraction", fontsize=16,
    )

    axc.annotate(
        'C', xy=(-.3, 1.), color='k', weight='bold',
        xycoords="axes fraction", fontsize=16,
    )

    tbl = df.drop(['day', 'month', 'year', 'place_of_occurrence', 'external_cause_of_death'], axis=1)
    tbl = tbl.groupby(['tpu'], as_index=True).count()
    tbl.sort_values(by=['age'], ascending=False, inplace=True)
    tbl = tbl.div(tbl.sum(), axis='columns')

    features = [
        f.upper() if len(f.split('_')) == 1 else ''.join([i[0] for i in f.split('_')]).upper()
        for f in tbl.columns
    ]

    vmin = .001
    vmax = 0.01
    step = .001
    bounds = np.arange(vmin, vmax+step, step=step)
    xlabels = [f'{x}' for x in bounds]
    cmap = plt.cm.get_cmap('viridis_r')
    cmaplist = [cmap(i) for i in range(cmap.N)]
    cmaplist[0] = (1, 1, 1, 1.0)  # force the first color entry to be white
    cmap = mcolors.LinearSegmentedColormap.from_list(None, cmaplist, cmap.N)
    norm = mcolors.BoundaryNorm(bounds, cmap.N)

    cbarax = inset_axes(
        ax,
        width="100%",
        height="2%",
        bbox_to_anchor=(0, .05, 1, 1),
        bbox_transform=ax.transAxes,
        borderpad=.25,
    )

    ax = sns.heatmap(
        tbl,
        ax=ax,
        cbar=True,
        cbar_ax=cbarax,
        cmap=cmap,
        norm=norm,
        vmin=vmin,
        vmax=vmax,
        linewidths=0.01,
        linecolor='k',
        yticklabels=[],
        xticklabels=features
    )
    cb = colorbar.ColorbarBase(
        cbarax,
        cmap=cmap,
        norm=norm,
        spacing='proportional',
        ticks=bounds,
        boundaries=bounds,
        orientation='horizontal',
        extend='both'
    )

    cbarax.tick_params(labelsize=14)
    cbarax.set_xticklabels(xlabels)
    cbarax.xaxis.set_ticks_position('top')
    cbarax.yaxis.set_label_position('left')

    ax.tick_params(axis='x', rotation=45)
    ax.set_ylabel('Fraction of records by TPU')

    tpus = df.groupby(['tpu'], as_index=False).size().to_frame('total')
    tpus['rank'] = tpus['total'].rank(method='average', ascending=False)
    tpus['freq'] = tpus['total'] / tpus['total'].sum()
    tpus = tpus.sort_values(by='total', ascending=False)

    axb.loglog(
        tpus['rank'],
        tpus['freq'],
        marker='o',
        ms=3,
        color='grey',
        mfc='grey',
        mec='grey',
        lw=0,
    )

    fit = np.polyfit(np.log(tpus['rank']), tpus['freq'], 1)
    fit = fit[0] * np.log(tpus['rank']) + fit[1]
    axb.loglog(tpus['rank'], fit, color='r')

    axb.set_xlabel('TPUs')
    axb.set_ylabel('Fraction of records')
    axb.grid(True, which="both", axis='both', alpha=.3, lw=1, linestyle='-')
    axb.spines['right'].set_visible(False)
    axb.spines['top'].set_visible(False)

    ddf = df.groupby(['tpu', 'year'], as_index=False).size().to_frame('total').reset_index()
    ddf.year = pd.to_datetime(ddf.year.astype(int), format='%Y').dt.strftime('%Y')

    for y in ddf.year.unique():
        ts = ddf[ddf.year == y]
        ts['rank'] = ts['total'].rank(method='average', ascending=False)
        ts = ts.sort_values(by='total', ascending=False)

        axc.loglog(
            ts['rank'],
            ts['total'],
            marker='o',
            ms=3,
            color=consts.cmap[y],
            mfc=consts.cmap[y],
            mec=consts.cmap[y],
            lw=0,
            alpha=.15
        )

        axc.set_xlabel('TPUs')
        axc.set_ylabel('Annual number of deaths')
        axc.grid(True, which="both", axis='both', alpha=.3, lw=1, linestyle='-')
        axc.spines['right'].set_visible(False)
        axc.spines['top'].set_visible(False)

    for g, c in zip(consts.year_groups, consts.group_colors):
        avg = ddf[
            ddf['year'].isin(map(str, g))
        ].groupby(['tpu']).mean().reset_index()
        avg['rank'] = avg['total'].rank(method='average', ascending=False)
        avg = avg.sort_values(by='total', ascending=False)
        axc.loglog(avg['rank'], avg['total'], color=c)

    legend_colors = [
        Line2D([0], [0], color='C0', lw=0, marker='o', ms=5),
        Line2D([0], [0], color='C1', lw=0, marker='o', ms=5),
        Line2D([0], [0], color='C2', lw=0, marker='o', ms=5),
        Line2D([0], [0], color='k', lw=2)
    ]
    legend_labels = ["2010's", "2000's", "1990's", r'$E[y]$']

    axc.legend(
        legend_colors,
        legend_labels,
        frameon=False
    )
    axc.set_ylabel('Annual number of deaths')

    plt.tight_layout()
    plt.savefig(f'{savepath}.png', dpi=200, bbox_inches='tight', pad_inches=.25)
    plt.savefig(f'{savepath}.pdf', bbox_inches='tight', pad_inches=.25)


def plot_district_features(df, savepath):
    """Plot features for districts
    Args:
        df (pandas.Dataframe): a dataframe where rows are records and columns are features
        savepath (str): absolute path to save plot
    """

    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
    })

    fig = plt.figure(figsize=(10, 8))
    gs = fig.add_gridspec(ncols=3, nrows=2)
    ax = fig.add_subplot(gs[:, :2])
    axb = fig.add_subplot(gs[0, -1])
    axc = fig.add_subplot(gs[1, -1])

    ax.annotate(
        'A', xy=(-.3, 1.), color='k', weight='bold',
        xycoords="axes fraction", fontsize=16,
    )

    axb.annotate(
        'B', xy=(-.3, 1.), color='k', weight='bold',
        xycoords="axes fraction", fontsize=16,
    )

    axc.annotate(
        'C', xy=(-.3, 1.), color='k', weight='bold',
        xycoords="axes fraction", fontsize=16,
    )

    tbl = df.drop(['day', 'month', 'year', 'place_of_occurrence', 'external_cause_of_death', 'tpu'], axis=1)
    tbl = tbl.groupby(['district'], as_index=True).count()
    tbl.sort_values(by=['age'], ascending=False, inplace=True)
    tbl = tbl.div(tbl.sum(), axis='columns')

    features = [
        f.upper() if len(f.split('_')) == 1 else ''.join([i[0] for i in f.split('_')]).upper()
        for f in tbl.columns
    ]

    vmin = .01
    vmax = .1
    step = .01
    bounds = np.arange(vmin, vmax+step, step=step)
    xlabels = [f'{round(x, 2)}' for x in bounds]
    cmap = plt.cm.get_cmap('viridis_r')
    cmaplist = [cmap(i) for i in range(cmap.N)]
    cmaplist[0] = (1, 1, 1, 1.0)  # force the first color entry to be white
    cmap = mcolors.LinearSegmentedColormap.from_list(None, cmaplist, cmap.N)
    norm = mcolors.BoundaryNorm(bounds, cmap.N)

    cbarax = inset_axes(
        ax,
        width="100%",
        height="2%",
        bbox_to_anchor=(0, .05, 1, 1),
        bbox_transform=ax.transAxes,
        borderpad=.25,
    )

    ax = sns.heatmap(
        tbl,
        ax=ax,
        cbar=True,
        cbar_ax=cbarax,
        cmap=cmap,
        norm=norm,
        vmin=vmin,
        vmax=vmax,
        linewidths=0.01,
        linecolor='k',
        xticklabels=features
    )
    cb = colorbar.ColorbarBase(
        cbarax,
        cmap=cmap,
        norm=norm,
        spacing='proportional',
        ticks=bounds,
        boundaries=bounds,
        orientation='horizontal',
        extend='both'
    )

    cbarax.tick_params(labelsize=14)
    cbarax.set_xticklabels(xlabels)
    cbarax.xaxis.set_ticks_position('top')
    cbarax.yaxis.set_label_position('left')

    ax.tick_params(axis='x', rotation=45)
    ax.set_ylabel('Fraction of records by district')

    districts = df.groupby(['district'], as_index=False).size().to_frame('total')
    districts['rank'] = districts['total'].rank(method='average', ascending=False)
    districts['freq'] = districts['total'] / districts['total'].sum()
    districts = districts.sort_values(by='total', ascending=False)

    axb.semilogy(
        districts['rank'],
        districts['freq'],
        marker='o',
        ms=3,
        color='grey',
        mfc='grey',
        mec='grey',
        lw=0,
    )

    axb.set_xlabel('Districts')
    axb.set_ylabel('Fraction of records')
    axb.grid(True, which="both", axis='both', alpha=.3, lw=1, linestyle='-')
    axb.spines['right'].set_visible(False)
    axb.spines['top'].set_visible(False)

    ddf = df.groupby(['district', 'year'], as_index=False).size().to_frame('total').reset_index()
    ddf.year = pd.to_datetime(ddf.year.astype(int), format='%Y').dt.strftime('%Y')

    for y in ddf.year.unique():
        ts = ddf[ddf.year == y]
        ts['rank'] = ts['total'].rank(method='average', ascending=False)
        ts = ts.sort_values(by='total', ascending=False)

        axc.semilogy(
            ts['rank'],
            ts['total'],
            marker='o',
            ms=3,
            color=consts.cmap[y],
            mfc=consts.cmap[y],
            mec=consts.cmap[y],
            lw=0,
            alpha=.15
        )

        axc.set_xlabel('Districts')
        axc.set_ylabel('Annual number of deaths')
        axc.grid(True, which="both", axis='both', alpha=.3, lw=1, linestyle='-')
        axc.spines['right'].set_visible(False)
        axc.spines['top'].set_visible(False)

    for g, c in zip(consts.year_groups, consts.group_colors):
        avg = ddf[
            ddf['year'].isin(map(str, g))
        ].groupby(['district']).mean().reset_index()
        avg['rank'] = avg['total'].rank(method='average', ascending=False)
        avg = avg.sort_values(by='total', ascending=False)
        axc.semilogy(avg['rank'], avg['total'], color=c)

    legend_colors = [
        Line2D([0], [0], color='C0', lw=0, marker='o', ms=5),
        Line2D([0], [0], color='C1', lw=0, marker='o', ms=5),
        Line2D([0], [0], color='C2', lw=0, marker='o', ms=5),
        Line2D([0], [0], color='k', lw=2)
    ]
    legend_labels = ["2010's", "2000's", "1990's", r'$E[y]$']

    axc.legend(
        legend_colors,
        legend_labels,
        frameon=False
    )
    axc.set_ylabel('Annual number of deaths')

    plt.tight_layout()
    plt.savefig(f'{savepath}.png', dpi=200, bbox_inches='tight', pad_inches=.25)
    plt.savefig(f'{savepath}.pdf', bbox_inches='tight', pad_inches=.25)


def map(df, hk_map, savepath):
    """Plot fraction of missing data
    Args:
        df (geopandas.Dataframe): a geo pandas dataframe to plot the map
        savepath (str): absolute path to save plot
    """
    def gen_cmap(cmap, bounds):
        cmaplist = [cmap(i) for i in range(cmap.N)]
        #cmaplist[0] = (1, 1, 1, 1.0)  # force the first color entry to be white
        cmap = mcolors.LinearSegmentedColormap.from_list(None, cmaplist, cmap.N)
        norm = mcolors.BoundaryNorm(bounds, cmap.N)
        return cmap, norm

    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
    })
    years = df.year.unique()
    targets = ['population', 'mortality', 'life_insurance', 'unemployment', 'median_income']

    fig, axes = plt.subplots(figsize=(12, 16), ncols=len(years), nrows=len(targets))

    cbarax = inset_axes(
        axes[-1, 1],
        width="240%",
        height="8%",
        bbox_to_anchor=(1, -1, 1, 1),
        bbox_transform=axes[-1, 1].transAxes,
    )
    vmin, vmax = -1.75, 2
    bounds = np.arange(vmin, vmax, step=.25)
    cmap, norm = gen_cmap(plt.cm.get_cmap('RdGy_r'), bounds)

    for i, (t, label) in enumerate(zip(
        targets,
        ['Population', 'Mortality Rate', 'Life Insurance Rate', 'Unemployment Rate', 'Median Income']
    )):
        axes[i, 0].set_ylabel(label)

        for j, y in enumerate(years):
            axes[0, j].set_title(y)

            ax = axes[i, j]
            d = df[df.year == y][['district', t]].set_index('district')
            mean_ = d.mean()
            std_ = d.std()
            d = (d - mean_) / std_

            if t not in hk_map.columns:
                hk_map = hk_map.combine_first(d)
            else:
                hk_map.update(d)

            ax = hk_map.plot(
                column=t,
                ax=ax,
                alpha=0.75,
                edgecolor='k',
                legend=True,
                cmap=cmap,
                vmin=vmin,
                vmax=vmax,
                norm=norm,
                cax=cbarax,
                #scheme='quantiles'
            )

            ax.spines['right'].set_visible(False)
            ax.spines['top'].set_visible(False)
            ax.spines['left'].set_visible(False)
            ax.spines['bottom'].set_visible(False)
            ax.set_xticks([])
            ax.set_yticks([])

        for ax, tag in zip(axes.flatten(), consts.tags):
            ax.annotate(
                tag, xy=(.05, .85), color='k', weight='bold',
                xycoords="axes fraction", fontsize=16,
            )

    colorbar.ColorbarBase(
        cbarax,
        cmap=cmap,
        norm=norm,
        spacing='proportional',
        ticks=bounds,
        boundaries=bounds,
        orientation='horizontal',
        extend='both'
    )
    cbarax.tick_params(labelsize=12)
    cbarax.xaxis.set_ticks_position('bottom')
    cbarax.yaxis.set_label_position('left')
    cbarax.set_ylabel(r'Standard score = $\dfrac{X - \mu}{\sigma}$', rotation=0, labelpad=100, y=-.5)

    plt.tight_layout()
    plt.subplots_adjust(top=0.97, right=0.97, hspace=-0.5)
    plt.savefig(f'{savepath}.png', dpi=150, bbox_inches='tight', pad_inches=.25)
    plt.savefig(f'{savepath}.pdf', bbox_inches='tight', pad_inches=.25)
