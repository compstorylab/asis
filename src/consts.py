
import seaborn as sns

tags = 'A B C D E F G H I J K L M N O P Q R S T U V W X Y Z'.split(' ')

range_1 = range(2010, 2020)
range_2 = range(2000, 2010)
range_3 = range(1995, 2000)
year_groups = [range_1, range_2, range_3]
group_colors = ['C0', 'C1', 'C2']
group_cmaps = ['Blues', 'Oranges', 'Greens']


cmap = dict(zip(
    map(str, range_1),
    sns.color_palette(group_cmaps[0], len(range_1))
))
cmap.update(dict(zip(
    map(str, range_2),
    sns.color_palette(group_cmaps[1], len(range_2))
)))
cmap.update(dict(zip(
    map(str, range_3),
    sns.color_palette(group_cmaps[2], len(range_3))
)))

id_to_district = {
    0: "ISLANDS",
    1: "KWAI TSING",
    2: "NORTH",
    3: "SAI KUNG",
    4: "SHA TIN",
    5: "TAI PO",
    6: "TSUEN WAN",
    7: "TUEN MUN",
    8: "YUEN LONG",
    9: "KOWLOON CITY",
    10: "KWUN TONG",
    11: "SHAM SHUI PO",
    12: "WONG TAI SIN",
    13: "YAU TSIM MONG",
    14: "CENTRAL & WESTERN",
    15: "EASTERN",
    16: "SOUTHERN",
    17: "WAN CHAI",
}

doi = {
    2: "SAI KUNG",
    7: "SHA TIN",
    15: "WONG TAI SIN",
    16: "SOUTHERN",
}

wealth_vars = [
    'median_income',
    'median_rent_to_income_ratio',
    'median_monthly_household_income',
    'life_insurance',
]

age_vars = [
    'young_households',
    'elderly_households',
    'in_school',
]

race_vars = [
    'minorities',
    'unemployment_minorities',
]

base_vars = [
    'year',
    'district',
    'population',
    'mortality',
    'homeless',
    'unemployment',
    'unemployment_households',
    'mobile_residents',
    'single_parents',
]


voi = [
    'median_income',
    'median_rent_to_income_ratio',
    'life_insurance',
    'population',
    'minorities',
    'homeless',
    'unemployment_households',
    'unemployment_minorities',
    'mobile_residents',
    'single_parents',
    'young_households',
    'elderly_households',
]


base_models = dict(
    model_households_base=base_vars,
    model_households_wealth=base_vars + wealth_vars,
    model_households_all=base_vars + wealth_vars + race_vars + age_vars,
)

spatial_models = dict(
    spatial_model_households_base=base_vars,
    spatial_model_households_wealth=base_vars + wealth_vars,
    spatial_model_households_all=base_vars + wealth_vars + race_vars + age_vars,
)

models_codenames = {
    'Model': 'Baseline',
    'Spatial model': 'SP',
    'Weighted spatial model': 'WSP'
}

models_cmap = {code: color for code, color in zip(models_codenames, ['dimgrey', 'C0', 'C1'])}
models_ls = {code: color for code, color in zip(models_codenames, ['--', '-', '-.'])}
