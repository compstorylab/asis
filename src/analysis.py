
import sys
import time
from pathlib import Path

import cli
import consts
import corr
import geopandas
import heatmaps
import networks
import pandas as pd
import utils
import models


def parse_args(args):
    parser = cli.parser()

    # optional subparsers
    subparsers = parser.add_subparsers(help='Arguments for specific action.', dest='dtype')
    subparsers.required = True

    subparsers.add_parser(
        'crosstab',
        help='combine datasets at the district level'
    )

    subparsers.add_parser(
        'heatmap',
        help='plot a heatmap of Hong Kong'
    )

    subparsers.add_parser(
        'network',
        help='plot a network of Hong Kong'
    )

    subparsers.add_parser(
        'corr',
        help='pair-wise correlation'
    )

    subparsers.add_parser(
        'model',
        help='run a localized model of mortality risks'
    )

    subparsers.add_parser(
        'spatial_model',
        help='run a non-local model of mortality risks'
    )

    subparsers.add_parser(
        'all_models',
        help='run all models'
    )

    subparsers.add_parser(
        'plot_coff',
        help='plot distribution of coefficients for each model'
    )

    subparsers.add_parser(
        'plot_pred',
        help='plot mortality predictions by district'
    )

    subparsers.add_parser(
        'plot_error',
        help='plot mortality errors by district'
    )

    subparsers.add_parser(
        'plot_scale',
        help='plot mortality error scaling by district'
    )

    subparsers.add_parser(
        'plot_vars',
        help='plot a correlation heatmap between mortality '
             'and some variables of interest for each model'
    )

    subparsers.add_parser(
        'plot_doi',
        help='plot more information for districts of interest'
    )

    subparsers.add_parser(
        'plot_ego_networks',
        help='plot more information for districts of interest'
    )

    parser.add_argument(
        '--maps',
        default=Path('.').resolve().parent / 'maps',
        help='absolute Path to maps'
    )

    parser.add_argument(
        '--models',
        default=Path('.').resolve().parent / 'models',
        help='absolute Path to save models'
    )

    parser.add_argument(
        '--weighted',
        action='store_true',
        help='use a weighted adjacency matrix for the spatial model'
    )

    parser.add_argument(
        '-i', '--indir',
        default=Path('.').resolve().parent/'data',
        help='absolute Path to data directory'
    )

    parser.add_argument(
        '-o', '--outdir',
        default=Path('.').resolve().parent/'results',
        help='absolute Path to save results'
    )

    return parser.parse_args(args)


def main(args=None):
    timeit = time.time()

    if args is None:
        args = sys.argv[1:]

    args = parse_args(args)
    Path(args.outdir).mkdir(parents=True, exist_ok=True)

    crosstab_gender = utils.get_crosstabs(args.indir / 'crosstab_gender.csv')
    crosstab_gender['district'] = crosstab_gender['district'].str.upper()

    crosstab_households = utils.get_crosstabs(args.indir / 'crosstab_households.csv')
    crosstab_households['district'] = crosstab_households['district'].str.upper()

    hk_map = geopandas.read_file(f"zip:{args.maps}/HKG_districts.zip!/hong_kong_18_districts.shp")
    hk_map = hk_map.to_crs(epsg=3857)
    hk_map = hk_map.set_index('ENAME')

    ids = {district: i+1 for i, district in consts.id_to_district.items()}
    hk_map['OBJECTID'] = hk_map.index.map(ids)

    train_years = ('2006', '2011', '2016')

    if args.dtype == 'crosstab':
        mma = utils.get_mma_records(args.indir / 'mma_9518.csv')
        eco = utils.get_census_records(args.indir / 'districts_economic_0616.csv', index_col=[0, 1, 2])
        hou = utils.get_census_records(args.indir / 'districts_household_0616.csv', index_col=[0, 1])
        dea = utils.get_death_records(args.indir / 'rdeaths_9517.csv.gz')

        utils.crosstab_gender(dea, mma, eco, args.indir / 'crosstab_gender.csv')
        utils.crosstab_households(dea, mma, eco, hou, args.indir / 'crosstab_households.csv')

    elif args.dtype == 'network':
        adjmat = pd.read_csv(args.maps / 'adjacency_matrix.csv', header=0, index_col=0).fillna(0)
        net = networks.get_network(adjmat.values)
        wnet = networks.get_network(networks.update_weights(net))
        networks.plot_network(net, wnet, hk_map, f'{args.outdir}/network')

    elif args.dtype == 'heatmap':
        heatmaps.map(crosstab_households, hk_map, f'{args.outdir}/map')

    elif args.dtype == 'corr':
        corr.plot_corr(crosstab_households, f'{args.outdir}/corr')
        corr.plot_autocorrelation(crosstab_households, hk_map, f'{args.outdir}/autocorrelation')

    elif args.dtype == 'model':

        for name, variables in consts.base_models.items():
            df = models.prep_dataframe(crosstab_households, train_years, variables)

            models.fit_model(
                df,
                model=models.base_regression_model,
                savepath=args.models/name,
                train_years=train_years
            )

    elif args.dtype == 'spatial_model':
        adjmat = pd.read_csv(args.maps / 'adjacency_matrix.csv', header=0, index_col=0).fillna(0).values

        if args.weighted:
            net = networks.get_network(adjmat)
            adjmat = networks.update_weights(net)

        for name, variables in consts.spatial_models.items():
            df = models.prep_dataframe(crosstab_households, train_years, variables)

            models.fit_model(
                df,
                model=models.network_regression_model,
                savepath=args.models/f'weighted_{name}' if args.weighted else args.models/name,
                train_years=train_years,
                adjmat=adjmat,
            )

    elif args.dtype == 'all_models':
        adjmat = pd.read_csv(args.maps / 'adjacency_matrix.csv', header=0, index_col=0).fillna(0).values
        losses = {}

        for name, variables in consts.base_models.items():
            df = models.prep_dataframe(crosstab_households, train_years, variables)

            losses[name] = models.fit_model(
                df,
                model=models.base_regression_model,
                savepath=args.models/name,
                train_years=train_years,
            )

            losses[f'spatial_{name}'] = models.fit_model(
                df,
                model=models.network_regression_model,
                savepath=args.models/f'spatial_{name}',
                train_years=train_years,
                adjmat=adjmat,
            )

            net = networks.get_network(adjmat)
            adjmat = networks.update_weights(net)

            losses[f'weighted_spatial_{name}'] = models.fit_model(
                df,
                model=models.network_regression_model,
                savepath=args.models/f'weighted_spatial_{name}',
                train_years=train_years,
                adjmat=adjmat,
            )

        models.plot_loss(
            pd.DataFrame(losses),
            savepath=args.models/f'loss'
        )

    elif args.dtype == 'plot_coff':
        coffs = ['beta', 'gamma']
        try:
            for name, variables in consts.base_models.items():
                df = models.prep_dataframe(
                    crosstab_households,
                    train_years,
                    variables,
                    log_odds_ratio=True
                )

                models.plot_model_results(
                    df,
                    model=f'{args.models}/{name}.gz',
                    savepath=f'{args.outdir}/{name}',
                    coff='beta'
                )

                for coff in coffs:
                    models.plot_model_results(
                        df,
                        model=f'{args.models}/spatial_{name}.gz',
                        savepath=f'{args.outdir}/spatial_{name}',
                        coff=coff
                    )

                    models.plot_model_results(
                        df,
                        model=f'{args.models}/weighted_spatial_{name}.gz',
                        savepath=f'{args.outdir}/weighted_spatial_{name}',
                        coff=coff
                    )

        except FileNotFoundError:
            print('Please run: python analysis.py all_models')

    elif args.dtype == 'plot_pred':
        try:

            df = models.prep_dataframe(
                crosstab_households,
                train_years,
                consts.base_vars + consts.wealth_vars + consts.race_vars + consts.age_vars,
            )

            cats = ['base', 'wealth', 'all']
            for y in train_years:
                for c in cats:
                    models.plot_model_kde(
                        df,
                        models=args.models.glob(f'*_{c}.gz'),
                        savepath=f'{args.outdir}/kde_{c}_{y}',
                        year=y
                    )
                    models.plot_model_dists(
                        df,
                        models=args.models.glob(f'*_{c}.gz'),
                        savepath=f'{args.outdir}/predictions_{c}_{y}',
                        year=y,
                    )

        except FileNotFoundError:
            print('Please run: python analysis.py all_models')

    elif args.dtype == 'plot_error':
        try:

            df = models.prep_dataframe(
                crosstab_households,
                train_years,
                consts.base_vars + consts.wealth_vars + consts.race_vars + consts.age_vars,
            )

            cats = ['base', 'wealth', 'all']
            df_mean = pd.DataFrame()

            for y in train_years:
                dfs = []
                for c in cats:
                    dfs.append(models.plot_model_dists(
                        df,
                        models=args.models.glob(f'*_{c}.gz'),
                        savepath=f'{args.outdir}/errors_{c}_{y}',
                        year=y,
                        error=True
                    ))

                df_districts = pd.concat(dfs, axis=1, keys=['Base', 'Wealth', 'All'])
                df_mean[y] = df_districts.mean(axis=0)

                if y == '2016':

                    print(df_districts.round(2).to_latex())

            print(df_mean.T.round(2).to_latex())

        except FileNotFoundError:
            print('Please run: python analysis.py all_models')

    elif args.dtype == 'plot_scale':
        try:

            df = models.prep_dataframe(
                crosstab_households,
                train_years,
                consts.base_vars + consts.wealth_vars + consts.race_vars + consts.age_vars,
            )

            cats = ['base', 'wealth', 'all']
            for c in cats:
                results = []
                for y in train_years:
                    results.extend(models.plot_model_scale(
                        df,
                        models=args.models.glob(f'*_{c}.gz'),
                        savepath=f'{args.outdir}/scale_{c}_{y}',
                        year=y,
                    ))
                results = pd.DataFrame(results)
                results.to_csv(
                    f'{args.outdir}/scale_{c}_results.csv',
                    index=False
                )

        except FileNotFoundError:
            print('Please run: python analysis.py all_models')

    elif args.dtype == 'plot_vars':
        try:
            cats = ['base', 'wealth', 'all']
            for c in cats:
                results = pd.read_csv(
                    f'{args.outdir}/scale_{c}_results.csv',
                    header=0
                )
                models.plot_vars(
                    results,
                    savepath=f'{args.outdir}/vars_{c}',
                )

        except FileNotFoundError:
            print('Please run: python analysis.py plot_scale')

    elif args.dtype == 'plot_doi':
        net = networks.get_network(
            pd.read_csv(args.maps / 'adjacency_matrix.csv', header=0, index_col=0).fillna(0).values
        )

        try:

            df = models.prep_dataframe(
                crosstab_households,
                train_years,
                consts.base_vars + consts.wealth_vars + consts.race_vars + consts.age_vars,
            )

            cats = ['base', 'wealth', 'all']
            for y in train_years:
                for c in cats:
                    models.plot_doi(
                        df,
                        models=args.models.glob(f'*_{c}.gz'),
                        net=net,
                        savepath=f'{args.outdir}/doi_{c}_{y}',
                        year=y,
                    )

        except FileNotFoundError:
            print('Please run: python analysis.py all_models')

    elif args.dtype == 'plot_ego_networks':
        net = networks.get_network(
            pd.read_csv(args.maps / 'adjacency_matrix.csv', header=0, index_col=0).fillna(0).values
        )

        try:
            df = models.prep_dataframe(
                crosstab_households,
                train_years,
                consts.base_vars + consts.wealth_vars + consts.race_vars + consts.age_vars,
            )

            cats = ['base', 'wealth', 'all']
            for y in train_years:
                for c in cats:
                    models.plot_ego_networks(
                        df,
                        models=args.models.glob(f'*_{c}.gz'),
                        net=net,
                        savepath=f'{args.outdir}/influence_networks/ego_{c}_{y}',
                        year=y,
                    )

        except FileNotFoundError:
            print('Please run: python analysis.py all_models')

    else:
        print('Error: unknown action!')

    print(f'Total time elapsed: {time.time() - timeit:.2f} sec.')


if __name__ == "__main__":
    main()
